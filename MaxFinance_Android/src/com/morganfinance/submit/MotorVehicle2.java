package com.morganfinance.submit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.morganfinance.GlobalData;
import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

public class MotorVehicle2 {
	
	Activity parent;
	public View view;
	
	public static final int ERROR_NONE = -1;
	public static final int ERROR_GENERAL = 0;
	public static final int ERROR_CURRENT_VALUE2 = 1;
	public static final int ERROR_BRAND2 = 2;
	public static final int ERROR_MODEL2 = 3;
	
	public EditText edtCurrentValue2;
	public EditText edtBrand2;
	public EditText edtModel2;
	
	Spinner spnRegisteredOwnersName2;
	Spinner spnYear2;
	
	Button	btnInfCurrentValue2;
	Button	btnInfBrand2;
	Button	btnInfModel2;
	Button	btnInfRegisteredOwnersName2;
	
	Button	btnYesUnderFinance2;
	Button	btnNoUnderFinance2;
	Button 	btnInfUnderFinance2;
	
	static String strUnderFinance2 = "";
	
	public MotorVehicle2(Context context) {
		// TODO Auto-generated constructor stub
		parent = (Activity) context;
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
    	LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.motor_vehicle_layout2, null);
    	
    	edtCurrentValue2 = (EditText) view.findViewById(R.id.current_value_editText2);
    	edtBrand2 = (EditText) view.findViewById(R.id.brand_editText2);
    	edtModel2 = (EditText) view.findViewById(R.id.model_editText2);
    	
    	spnRegisteredOwnersName2 = (Spinner) view.findViewById(R.id.registered_owner_name_spinner2);
    	spnYear2 = (Spinner) view.findViewById(R.id.year_spinner2);
    	
    	btnInfCurrentValue2 = (Button) view.findViewById(R.id.inf_current_value_button2);
    	btnInfBrand2		= (Button) view.findViewById(R.id.inf_brand_button2);
    	btnInfModel2		= (Button) view.findViewById(R.id.inf_model_button2);
    	btnInfRegisteredOwnersName2		= (Button) view.findViewById(R.id.inf_registered_owner_name_button2);

    	btnYesUnderFinance2 = (Button) view.findViewById(R.id.yes_under_finance_button2);
    	btnNoUnderFinance2 = (Button) view.findViewById(R.id.no_under_finance_button2);
    	btnInfUnderFinance2 = (Button) view.findViewById(R.id.inf_under_finance_button2);
	}
	
	private void initEvent() {
		btnYesUnderFinance2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new AlertDialog.Builder(parent)
		        .setTitle("Finance Owing...")
		        .setMessage("Are you sure your vehicle has Finance Owing?")
		        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) { 
		                // continue with delete
						strUnderFinance2 = "Yes";
						btnNoUnderFinance2.setBackgroundResource(R.drawable.no_normal);
						btnYesUnderFinance2.setBackgroundResource(R.drawable.yes_sel_middle);
		            }
		         })
		         .setNegativeButton("No", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) { 
		                // continue with delete
						strUnderFinance2 = "No";
						btnNoUnderFinance2.setBackgroundResource(R.drawable.no_sel);
						btnYesUnderFinance2.setBackgroundResource(R.drawable.yes_normal_middle);
		            }
		         })
		         .show();			}
	    });
		btnNoUnderFinance2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				strUnderFinance2 = "No";
				btnNoUnderFinance2.setBackgroundResource(R.drawable.no_sel);
				btnYesUnderFinance2.setBackgroundResource(R.drawable.yes_normal_middle);
			}
	    });
		btnInfUnderFinance2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "If a loan has not be taken out before, click No");
			}
	    });
		

		///////////// Information  ///////////////////////////////
		
		btnInfCurrentValue2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Current market value.");
			}
	    });
		btnInfBrand2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "e.g. Toyota, Holden, BMW, etc.");
			}
	    });
		btnInfModel2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "e.g. Camry, Barina, M6, etc.");
			}
	    });
		btnInfRegisteredOwnersName2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Do you have money owing against the vehicle? or have you taken" +
						" out a loan with this car as security?");
			}
	    });
	}
	
	private void initValue() {
		
		ArrayAdapter<String> spinnerArrayAdapter;
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfRegisteredOwnersNameForMortorVehicle);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnRegisteredOwnersName2.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfYear);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnYear2.setAdapter(spinnerArrayAdapter);
		
		loadSavedPreferences();
	}
	
	public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(parent)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            }
         })
         .show();
    }
	
    public void loadSavedPreferences() {
    	
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	
    	 String str;
         int n;
    	
         str = sharedPreferences.getString("strVehcieValue2", "");
         edtCurrentValue2.setText(str);
         str = sharedPreferences.getString("strMotorMake2", "");
         edtBrand2.setText(str);
         str = sharedPreferences.getString("strMotorModel2", "");
         edtModel2.setText(str);
         n = sharedPreferences.getInt("nVehicleOwner2", 0);
         spnRegisteredOwnersName2.setSelection(n);
         str = sharedPreferences.getString("strMotorFinance2", "");
         strUnderFinance2 = str;
         if (str.equals("Yes")) {
        	 btnNoUnderFinance2.setBackgroundResource(R.drawable.no_normal);
        	 btnYesUnderFinance2.setBackgroundResource(R.drawable.yes_sel_middle);
         }
         if (str.equals("No")) {
			btnNoUnderFinance2.setBackgroundResource(R.drawable.no_sel);
			btnYesUnderFinance2.setBackgroundResource(R.drawable.yes_normal_middle);
         }
         n = sharedPreferences.getInt("nMotorYear2", 0);
         spnYear2.setSelection(n);
    }
    
    public void savePreferences() {
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	
   	 	editor.putString	("strVehcieValue2", 		edtCurrentValue2.getText().toString());
   	 	editor.putString	("strMotorMake2", 		edtBrand2.getText().toString());
   	 	editor.putString	("strMotorModel2", 		edtModel2.getText().toString());
   	 	editor.putInt		("nVehicleOwner2", 	spnRegisteredOwnersName2.getSelectedItemPosition());
   	 	editor.putString	("strMotorFinance2", 		strUnderFinance2);
   	 	editor.putInt		("nMotorYear2", 	spnYear2.getSelectedItemPosition());
   	 	
   	 	editor.commit();
    }
	
	public int checkInput() {
		
		String str;
		
		str = edtCurrentValue2.getText().toString();
		if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Current Value of Motor Vehicle 2!");
    		edtCurrentValue2.requestFocus();
    		return ERROR_CURRENT_VALUE2;
    	}
		str = edtBrand2.getText().toString();
		if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Brand of Motor Vehicle 2!");
    		edtBrand2.requestFocus();
    		return ERROR_BRAND2;
    	}
		str = edtModel2.getText().toString();
		if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Model of Motor Vehicle 2!");
    		edtModel2.requestFocus();
    		return ERROR_MODEL2;
    	}
		str = GlobalData.itemsOfRegisteredOwnersNameForMortorVehicle[(int) spnRegisteredOwnersName2.getSelectedItemId()];
    	if (str == "-Select-") {
    		showGeneralAlert("Select", "Please select a Registered Owners Name of Motor Vehicle 2!");
    		spnRegisteredOwnersName2.requestFocus();
    		
    		return ERROR_GENERAL;
    	}
    	if (strUnderFinance2.equals("")) {
    		showGeneralAlert("Question", "Under Finance of Motor Vehicle 2?");
    		
    		return ERROR_GENERAL;
    	}
		str = GlobalData.itemsOfYear[(int) spnYear2.getSelectedItemId()];
    	if (str == "-Select-") {
    		showGeneralAlert("Select", "Please select an Year of Motor Vehicle 2!");
    		spnYear2.requestFocus();
    		
    		return ERROR_GENERAL;
    	}
    	
    	GlobalData.personalInfo.strVehcieValue2 = edtCurrentValue2.getText().toString();
    	GlobalData.personalInfo.strMotorMake2 = edtBrand2.getText().toString();
    	GlobalData.personalInfo.strMotorModel2 = edtModel2.getText().toString();
    	GlobalData.personalInfo.strVehicleOwner2 = GlobalData.itemsOfRegisteredOwnersNameForMortorVehicle[(int) spnRegisteredOwnersName2.getSelectedItemId()];
    	GlobalData.personalInfo.strMotorFinance2 = strUnderFinance2;
    	GlobalData.personalInfo.strMotorYear2 = GlobalData.itemsOfYear[(int) spnYear2.getSelectedItemId()];
    	
    	return ERROR_NONE;
	}
}
