package com.morganfinance.submit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.morganfinance.GlobalData;
import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

public class RealEstate2 {

	Activity parent;
	public View view;
	
	public static final int ERROR_NONE = -1;
	public static final int ERROR_GENERAL = 0;
	public static final int ERROR_MARKET_VALUE2 = 1;
	public static final int ERROR_ADDRESS2 = 2;
	
	public EditText edtMarketValue2;
	public EditText edtAddress2;
	
	Button btnNoAnyMortgageArrear2;
	Button btnYesAnyMortgageArrear2;
	
	Button btnInfMarketValue2;
	Button btnInfRegisteredOwnersName2;
	
	Spinner spnRegisteredOwnersName2;
	
	String strAnyMortgageArrear2 = "";

	public RealEstate2(Context context) {
		// TODO Auto-generated constructor stub
		parent = (Activity) context;
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.real_estate_layout2, null);
    	
    	edtMarketValue2 = (EditText) view.findViewById(R.id.market_value_editText2);
    	edtAddress2		= (EditText) view.findViewById(R.id.address_editText2);
    	
    	btnNoAnyMortgageArrear2 = (Button) view.findViewById(R.id.no_any_mortgage_button2);
    	btnYesAnyMortgageArrear2 = (Button) view.findViewById(R.id.yes_any_mortgage_button2);
    	
    	spnRegisteredOwnersName2 = (Spinner) view.findViewById(R.id.registered_owner_name_spinner2);
    	
    	btnInfMarketValue2 = (Button) view.findViewById(R.id.inf_market_value_button2);
    	btnInfRegisteredOwnersName2 = (Button) view.findViewById(R.id.inf_registered_owner_name_button2);
	}
	
	private void initEvent() {
		btnNoAnyMortgageArrear2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				strAnyMortgageArrear2 = "No";
				btnNoAnyMortgageArrear2.setBackgroundResource(R.drawable.no_sel);
				btnYesAnyMortgageArrear2.setBackgroundResource(R.drawable.yes_normal);
			}
	    });
		btnYesAnyMortgageArrear2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				strAnyMortgageArrear2 = "Yes";
				btnNoAnyMortgageArrear2.setBackgroundResource(R.drawable.no_normal);
				btnYesAnyMortgageArrear2.setBackgroundResource(R.drawable.yes_sel);
			}
	    });
		
		////////////////   Information
		btnInfMarketValue2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Current market value.");
			}
	    });
		btnInfRegisteredOwnersName2.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Living Expenses for you and your dependants " +
						"(excluding rental/mortgage payment)\n" +
						"\t Utilities cost\n" +
						"\t Food (include take-away)\n" +
						"\t Health (e.g. Doctor, Medicines, Others)\n" +
						"\t Insurance (e.g. health, car insurance)\n" +
						"\t Education (e.g. School fees, childcare)\n" +
						"\t Transport\n" +
						"\t Clothing\n" +
						"\t Entertainment\n" +
						"To assist you with your calculation, you may refer to ASIC�s Money Smart Budget Planner."
				);
			}
	    });
	}
	
	private void initValue() {
		ArrayAdapter<String> spinnerArrayAdapter;
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfRegisteredOwnersNameForRealEstate);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnRegisteredOwnersName2.setAdapter(spinnerArrayAdapter);
		
		loadSavedPreferences();
	}
	
	public void loadSavedPreferences() {
    	
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	
    	 String str;
         int n;
    	    	
         str = sharedPreferences.getString("strMarketValue2", "");
         edtMarketValue2.setText(str);
         str = sharedPreferences.getString("strPropertyAddress2", "");
         edtAddress2.setText(str);
         
         str = sharedPreferences.getString("strMorarr2", "");
         
         strAnyMortgageArrear2 = str;
         if (str == "Yes") {
				btnNoAnyMortgageArrear2.setBackgroundResource(R.drawable.no_normal);
				btnYesAnyMortgageArrear2.setBackgroundResource(R.drawable.yes_sel);
         }
         if (str == "No") {
				btnNoAnyMortgageArrear2.setBackgroundResource(R.drawable.no_sel);
				btnYesAnyMortgageArrear2.setBackgroundResource(R.drawable.yes_normal);
         }
        
         n = sharedPreferences.getInt("nPropertyOwner1", 0);
         spnRegisteredOwnersName2.setSelection(n);
    }
    
    public void savePreferences() {
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	
   	 	editor.putString	("strMarketValue2", 		edtMarketValue2.getText().toString());
   	 	editor.putString	("strPropertyAddress2", 	edtAddress2.getText().toString());
   	 	editor.putString	("strMorarr2", 				strAnyMortgageArrear2);
   	 	editor.putInt		("nPropertyOwner2", 		spnRegisteredOwnersName2.getSelectedItemPosition());

   	 	editor.commit();
    }
	
	public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(parent)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            }
         })
         .show();
    }
	
	public int checkInput() {
		String str;
		
		str = edtMarketValue2.getText().toString();
		if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Market Value of Real Estate 2!");
    		edtMarketValue2.requestFocus();
    		
    		return ERROR_MARKET_VALUE2;
    	}
		str = edtAddress2.getText().toString();
		if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Address of Real Estate 2!");
    		
    		return ERROR_ADDRESS2;
    	}
		if (strAnyMortgageArrear2.equals("")) {
    		showGeneralAlert("Question", "Any Mortgage Arrear of Real Estate 2?");
    		
    		return ERROR_GENERAL;
		}
		str = GlobalData.itemsOfRegisteredOwnersNameForRealEstate[(int) spnRegisteredOwnersName2.getSelectedItemId()];
    	if (str == "-Select-") {
    		showGeneralAlert("Select", "Please select an Registered Owner/s Name of Real Estate 2!");
    		spnRegisteredOwnersName2.requestFocus();
    		
    		return ERROR_GENERAL;
    	}
    	
    	GlobalData.personalInfo.strMarketValue2 = edtMarketValue2.getText().toString();
    	GlobalData.personalInfo.strPropertyAddress2 = edtAddress2.getText().toString();
    	GlobalData.personalInfo.strMorarr2 = strAnyMortgageArrear2;
    	GlobalData.personalInfo.strPropertyOwner2 = GlobalData.itemsOfRegisteredOwnersNameForRealEstate[(int) spnRegisteredOwnersName2.getSelectedItemId()];
 
    	return ERROR_NONE;
	}
}
