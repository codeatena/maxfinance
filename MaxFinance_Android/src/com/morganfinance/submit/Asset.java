package com.morganfinance.submit;

import com.example.morganfinance.GlobalData;
import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;
import com.example.morganfinance.SubmitActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class Asset {

	SubmitActivity parent;
	public View view;
	
	Spinner			spnNumberOfMotorVehicle;
	Spinner			spnNumberOfRealEstate;
	
	int 			nIndexOfNumberOfMotorVehicle;
	int 			nIndexOfNumberOfRealEstate;

	public Asset(Context context) {
		// TODO Auto-generated constructor stub
		parent = (SubmitActivity) context;
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
    	LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.asset_layout, null);
    	
    	spnNumberOfMotorVehicle = (Spinner) view.findViewById(R.id.number_of_motor_vehicle_spinner);
    	spnNumberOfRealEstate = (Spinner) view.findViewById(R.id.work_income_after_tax_spinner);
	}

	private void initEvent() {
		
		spnNumberOfMotorVehicle.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		        // your code here
		    	if (position == 0) {
		    		if (nIndexOfNumberOfRealEstate == 0) {
		    			showGeneralAlert("Error", "You must have at least one asset");
		    			spnNumberOfMotorVehicle.setSelection(nIndexOfNumberOfMotorVehicle);
		    			return;
		    		} 
			    	parent.btnMotorVehicle.setVisibility(View.GONE);
		    	} else {
		    		parent.btnMotorVehicle.setVisibility(View.VISIBLE);
		    	}
		    	nIndexOfNumberOfMotorVehicle = position;
		    	GlobalData.personalInfo.countMotorVehicle = position;
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		        // your code here
		    }
		});
		
		spnNumberOfRealEstate.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		        // your code here
		    	if (position == 0) {
		    		if (nIndexOfNumberOfMotorVehicle == 0) {
		    			showGeneralAlert("Error", "You must have at least one asset");
		    			spnNumberOfRealEstate.setSelection(nIndexOfNumberOfRealEstate);
		    			return;
		    		}
		    		parent.btnRealEstate.setVisibility(View.GONE);
		    	} else {
		    		parent.btnRealEstate.setVisibility(View.VISIBLE);
		    	}
		    	nIndexOfNumberOfRealEstate = position;
		    	GlobalData.personalInfo.countRealState = position;
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		        // your code here
		    }

		});
	}
	
	private void initValue() {
		ArrayAdapter<String> spinnerArrayAdapter;
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfNumberMotorVehicle);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnNumberOfMotorVehicle.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfNumberRealEstate);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnNumberOfRealEstate.setAdapter(spinnerArrayAdapter);
		
		loadSavedPreferences();
		
		spnNumberOfMotorVehicle.setSelection(1);
		spnNumberOfRealEstate.setSelection(0);
		
		nIndexOfNumberOfMotorVehicle = 1;
		nIndexOfNumberOfRealEstate = 0;
	}
	
	public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(parent)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
            }
         })
         .show();
    }
    
	public boolean checkInput() {
		
//		String str;
//		str = GlobalData.itemsOfNumberMotorVehicle[(int) spnNumberOfMotorVehicle.getSelectedItemId()];
//    	if (str == "None") {
//    		showGeneralAlert("Select", "Please select a Number of Motor Vehicle!");
//    		spnNumberOfMotorVehicle.requestFocus();
//    		return false;
//    	}
//		str = GlobalData.itemsOfNumberRealEstate[(int) spnNumberOfRealEstate.getSelectedItemId()];
//    	if (str == "None") {
//    		showGeneralAlert("Select", "Please select a Number of Real Estate!");
//    		spnNumberOfRealEstate.requestFocus();
//    		return false;
//    	}
    	
    	return true;
	}
	
    public void loadSavedPreferences() {
    	
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	
    	 String str;
         int n;
         
         n = sharedPreferences.getInt("nNumberOfMotorVehicle", 1);
         spnNumberOfMotorVehicle.setSelection(n);
         n = sharedPreferences.getInt("nNumberOfRealEstate", 0);
         spnNumberOfRealEstate.setSelection(n);
    }
    
    public void savePreferences() {
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	SharedPreferences.Editor editor = sharedPreferences.edit();
   	 	editor.putInt		("nNumberOfMotorVehicle", 	spnNumberOfMotorVehicle.getSelectedItemPosition());
   	 	editor.putInt		("nNumberOfRealEstate", 	spnNumberOfRealEstate.getSelectedItemPosition());
   	 	editor.commit();
    }
}
