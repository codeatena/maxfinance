package com.morganfinance.submit;

import com.example.morganfinance.GlobalData;
import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class RealEstate1 {
	
	Activity parent;
	public View view;
	
	public static final int ERROR_NONE = -1;
	public static final int ERROR_GENERAL = 0;
	public static final int ERROR_MARKET_VALUE1 = 1;
	public static final int ERROR_ADDRESS1 = 2;
	
	public EditText edtMarketValue1;
	public EditText edtAddress1;
	
	Button btnNoAnyMortgageArrear1;
	Button btnYesAnyMortgageArrear1;
	
	Button btnInfMarketValue1;
	Button btnInfRegisteredOwnersName1;
	
	Spinner spnRegisteredOwnersName1;
	
	String strAnyMortgageArrear1 = "";
	
	public RealEstate1(Context context) {
		// TODO Auto-generated constructor stub
		parent = (Activity) context;
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.real_estate_layout1, null);
    	
    	edtMarketValue1 = (EditText) view.findViewById(R.id.market_value_editText1);
    	edtAddress1		= (EditText) view.findViewById(R.id.address_editText1);
    	
    	btnNoAnyMortgageArrear1 = (Button) view.findViewById(R.id.no_any_mortgage_button1);
    	btnYesAnyMortgageArrear1 = (Button) view.findViewById(R.id.yes_any_mortgage_button1);
    	
    	spnRegisteredOwnersName1 = (Spinner) view.findViewById(R.id.registered_owner_name_spinner1);
    	
    	btnInfMarketValue1 = (Button) view.findViewById(R.id.inf_market_value_button1);
    	btnInfRegisteredOwnersName1 = (Button) view.findViewById(R.id.inf_registered_owner_name_button1);
	}
	
	private void initEvent() {
		btnNoAnyMortgageArrear1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				strAnyMortgageArrear1 = "No";
				btnNoAnyMortgageArrear1.setBackgroundResource(R.drawable.no_sel);
				btnYesAnyMortgageArrear1.setBackgroundResource(R.drawable.yes_normal);
			}
	    });
		btnYesAnyMortgageArrear1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				strAnyMortgageArrear1 = "Yes";
				btnNoAnyMortgageArrear1.setBackgroundResource(R.drawable.no_normal);
				btnYesAnyMortgageArrear1.setBackgroundResource(R.drawable.yes_sel);
			}
	    });
		
		////////////////   Information
		btnInfMarketValue1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Current market value.");
			}
	    });
		btnInfRegisteredOwnersName1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Living Expenses for you and your dependants " +
						"(excluding rental/mortgage payment)\n" +
						"\t Utilities cost\n" +
						"\t Food (include take-away)\n" +
						"\t Health (e.g. Doctor, Medicines, Others)\n" +
						"\t Insurance (e.g. health, car insurance)\n" +
						"\t Education (e.g. School fees, childcare)\n" +
						"\t Transport\n" +
						"\t Clothing\n" +
						"\t Entertainment\n" +
						"To assist you with your calculation, you may refer to ASIC�s Money Smart Budget Planner."
				);
			}
	    });
	}
	
	private void initValue() {
		ArrayAdapter<String> spinnerArrayAdapter;
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfRegisteredOwnersNameForRealEstate);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnRegisteredOwnersName1.setAdapter(spinnerArrayAdapter);
		
		loadSavedPreferences();
	}
	
	public void loadSavedPreferences() {
    	
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	
    	 String str;
         int n;
    	    	
         str = sharedPreferences.getString("strMarketValue1", "");
         edtMarketValue1.setText(str);
         str = sharedPreferences.getString("strPropertyAddress1", "");
         edtAddress1.setText(str);
         
         str = sharedPreferences.getString("strMorarr1", "");
         strAnyMortgageArrear1 = str;
         if (str == "Yes") {
				btnNoAnyMortgageArrear1.setBackgroundResource(R.drawable.no_normal);
				btnYesAnyMortgageArrear1.setBackgroundResource(R.drawable.yes_sel);
         }
         if (str == "No") {
				btnNoAnyMortgageArrear1.setBackgroundResource(R.drawable.no_sel);
				btnYesAnyMortgageArrear1.setBackgroundResource(R.drawable.yes_normal);
         }
        
         
         n = sharedPreferences.getInt("nPropertyOwner1", 0);
         spnRegisteredOwnersName1.setSelection(n);
    }
    
    public void savePreferences() {
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	
   	 	editor.putString	("strMarketValue1", 		edtMarketValue1.getText().toString());
   	 	editor.putString	("strPropertyAddress1", 	edtAddress1.getText().toString());
   	 	editor.putString	("strMorarr1", 				strAnyMortgageArrear1);
   	 	editor.putInt		("nPropertyOwner1", 		spnRegisteredOwnersName1.getSelectedItemPosition());

   	 	editor.commit();
    }
	
	public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(parent)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            }
         })
         .show();
    }
	
	public int checkInput() {
		String str;
		
		str = edtMarketValue1.getText().toString();
		if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Market Value of Real Estate 1!");
    		
    		return ERROR_MARKET_VALUE1;
    	}
		str = edtAddress1.getText().toString();
		if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Address of Real Estate 1!");
    		
    		return ERROR_ADDRESS1;
    	}
		if (strAnyMortgageArrear1.equals("")) {
    		showGeneralAlert("Question", "Any Mortgage Arrear of Real Estate 1?");
    		
    		return ERROR_GENERAL;
		}
		str = GlobalData.itemsOfRegisteredOwnersNameForRealEstate[(int) spnRegisteredOwnersName1.getSelectedItemId()];
    	if (str == "-Select-") {
    		showGeneralAlert("Select", "Please select an Registered Owner/s Name of Real Estate 1!");
    		
    		return ERROR_GENERAL;
    	}

    	GlobalData.personalInfo.strMarketValue1 = edtMarketValue1.getText().toString();
    	GlobalData.personalInfo.strPropertyAddress1 = edtAddress1.getText().toString();
    	GlobalData.personalInfo.strMorarr1 = strAnyMortgageArrear1;
    	GlobalData.personalInfo.strPropertyOwner1 = GlobalData.itemsOfRegisteredOwnersNameForRealEstate[(int) spnRegisteredOwnersName1.getSelectedItemId()];
    	
    	return ERROR_NONE;
	}
}
