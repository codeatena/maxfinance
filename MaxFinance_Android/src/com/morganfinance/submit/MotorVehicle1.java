package com.morganfinance.submit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.morganfinance.GlobalData;
import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

public class MotorVehicle1 {
	Activity parent;
	public View view;
	
	public static final int ERROR_NONE = -1;
	public static final int ERROR_GENERAL = 0;
	public static final int ERROR_CURRENT_VALUE1 = 1;
	public static final int ERROR_BRAND1 = 2;
	public static final int ERROR_MODEL1 = 3;
	
	public EditText edtCurrentValue1;
	public EditText edtBrand1;
	public EditText edtModel1;
	
	Spinner spnRegisteredOwnersName1;
	Spinner spnYear1;
	
	Button	btnInfCurrentValue1;
	Button	btnInfBrand1;
	Button	btnInfModel1;
	Button	btnInfRegisteredOwnersName1;
	
	Button	btnYesUnderFinance1;
	Button	btnNoUnderFinance1;
	Button 	btnInfUnderFinance1;
	
	static String strUnderFinance1 = "";
	
	public MotorVehicle1(Context context) {
		// TODO Auto-generated constructor stub
		parent = (Activity) context;
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
    	LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.motor_vehicle_layout1, null);
    	
    	edtCurrentValue1 = (EditText) view.findViewById(R.id.current_value_editText1);
    	edtBrand1 = (EditText) view.findViewById(R.id.brand_editText1);
    	edtModel1 = (EditText) view.findViewById(R.id.model_editText1);
    	
    	spnRegisteredOwnersName1 = (Spinner) view.findViewById(R.id.registered_owner_name_spinner1);
    	spnYear1 = (Spinner) view.findViewById(R.id.year_spinner1);
    	
    	btnInfCurrentValue1 = (Button) view.findViewById(R.id.inf_current_value_button1);
    	btnInfBrand1		= (Button) view.findViewById(R.id.inf_brand_button1);
    	btnInfModel1		= (Button) view.findViewById(R.id.inf_model_button1);
    	btnInfRegisteredOwnersName1		= (Button) view.findViewById(R.id.inf_registered_owner_name_button1);

    	btnYesUnderFinance1 = (Button) view.findViewById(R.id.yes_under_finance_button1);
    	btnNoUnderFinance1 = (Button) view.findViewById(R.id.no_under_finance_button1);
    	btnInfUnderFinance1 = (Button) view.findViewById(R.id.inf_under_finance_button1);
	}
	
	private void initEvent() {
		
		btnYesUnderFinance1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				new AlertDialog.Builder(parent)
		        .setTitle("Finance Owing...")
		        .setMessage("Are you sure your vehicle has Finance Owing?")
		        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) { 
		                // continue with delete
						strUnderFinance1 = "Yes";
						btnNoUnderFinance1.setBackgroundResource(R.drawable.no_normal);
						btnYesUnderFinance1.setBackgroundResource(R.drawable.yes_sel_middle);
		            }
		         })
		         .setNegativeButton("No", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) { 
		                // continue with delete
						strUnderFinance1 = "No";
						btnNoUnderFinance1.setBackgroundResource(R.drawable.no_sel);
						btnYesUnderFinance1.setBackgroundResource(R.drawable.yes_normal_middle);
		            }
		         })
		         .show();
			}
	    });
		btnNoUnderFinance1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				strUnderFinance1 = "No";
				btnNoUnderFinance1.setBackgroundResource(R.drawable.no_sel);
				btnYesUnderFinance1.setBackgroundResource(R.drawable.yes_normal_middle);
			}
	    });
		btnInfUnderFinance1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "If a loan has not be taken out before, click No");
			}
	    });
		
		
		///////////// Information  ///////////////////////////////
		
		btnInfCurrentValue1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Current market value.");
			}
	    });
		btnInfBrand1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "e.g. Toyota, Holden, BMW, etc.");
			}
	    });
		btnInfModel1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "e.g. Camry, Barina, M6, etc.");
			}
	    });
		btnInfRegisteredOwnersName1.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Do you have money owing against the vehicle? or have you taken" +
						" out a loan with this car as security?");
			}
	    });
	}
	
	private void initValue() {
		ArrayAdapter<String> spinnerArrayAdapter;
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfRegisteredOwnersNameForMortorVehicle);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnRegisteredOwnersName1.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfYear);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnYear1.setAdapter(spinnerArrayAdapter);
		
		loadSavedPreferences();
	}
	
	public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(parent)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            }
         })
         .show();
    }
	
    public void loadSavedPreferences() {
    	
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	
    	 String str;
         int n;
    	
         str = sharedPreferences.getString("strVehcieValue1", "");
         edtCurrentValue1.setText(str);
         str = sharedPreferences.getString("strMotorMake1", "");
         edtBrand1.setText(str);
         str = sharedPreferences.getString("strMotorModel1", "");
         edtModel1.setText(str);
         n = sharedPreferences.getInt("nVehicleOwner1", 0);
         spnRegisteredOwnersName1.setSelection(n);
         str = sharedPreferences.getString("strMotorFinance1", "");
         strUnderFinance1 = str;
         if (str.equals("Yes")) {
        	 btnNoUnderFinance1.setBackgroundResource(R.drawable.no_normal);
        	 btnYesUnderFinance1.setBackgroundResource(R.drawable.yes_sel_middle);
         }
         if (str.equals("No")) {
			btnNoUnderFinance1.setBackgroundResource(R.drawable.no_sel);
			btnYesUnderFinance1.setBackgroundResource(R.drawable.yes_normal_middle);
         }
         n = sharedPreferences.getInt("nMotorYear1", 0);
         spnYear1.setSelection(n);
    }
    
    public void savePreferences() {
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	
   	 	editor.putString	("strVehcieValue1", 		edtCurrentValue1.getText().toString());
   	 	editor.putString	("strMotorMake1", 		edtBrand1.getText().toString());
   	 	editor.putString	("strMotorModel1", 		edtModel1.getText().toString());
   	 	editor.putInt		("nVehicleOwner1", 	spnRegisteredOwnersName1.getSelectedItemPosition());
   	 	editor.putString	("strMotorFinance1", 		strUnderFinance1);
   	 	editor.putInt		("nMotorYear1", 	spnYear1.getSelectedItemPosition());
   	 	editor.commit();
    }

	public int checkInput() {
		
		String str;
		
		str = edtCurrentValue1.getText().toString();
		if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Current Value of Motor Vehicle 1!");
    		edtCurrentValue1.requestFocus();
    		return ERROR_CURRENT_VALUE1;
    	}
		str = edtBrand1.getText().toString();
		if (str.equals("")) {
    		edtBrand1.requestFocus();
    		showGeneralAlert("Input", "Please input Brand of Motor Vehicle 1!");
    		return ERROR_BRAND1;
    	}
		str = edtModel1.getText().toString();
		if (str.equals("")) {
    		edtModel1.requestFocus();
    		showGeneralAlert("Input", "Please input Model of Motor Vehicle 1!");
    		return ERROR_MODEL1;
    	}
		str = GlobalData.itemsOfRegisteredOwnersNameForMortorVehicle[(int) spnRegisteredOwnersName1.getSelectedItemId()];
    	if (str == "-Select-") {
    		showGeneralAlert("Select", "Please select a Registered Owners Name of Motor Vehicle 1!");
    		spnRegisteredOwnersName1.requestFocus();
    		
    		return ERROR_GENERAL;
    	}
    	if (strUnderFinance1.equals("")) {
    		showGeneralAlert("Question", "Under Finance of Motor Vehicle 1?");
    		
    		return ERROR_GENERAL;
    	}
		str = GlobalData.itemsOfYear[(int) spnYear1.getSelectedItemId()];
    	if (str == "-Select-") {
    		showGeneralAlert("Select", "Please select an Year of Motor Vehicle 1!");
    		spnYear1.requestFocus();
    		
    		return ERROR_GENERAL;
    	}
    	
    	GlobalData.personalInfo.strVehcieValue1 = edtCurrentValue1.getText().toString();
    	GlobalData.personalInfo.strMotorMake1 = edtBrand1.getText().toString();
    	GlobalData.personalInfo.strMotorModel1 = edtModel1.getText().toString();
    	GlobalData.personalInfo.strVehicleOwner1 = GlobalData.itemsOfRegisteredOwnersNameForMortorVehicle[(int) spnRegisteredOwnersName1.getSelectedItemId()];
    	GlobalData.personalInfo.strMotorFinance1 = strUnderFinance1;
    	GlobalData.personalInfo.strMotorYear1 = GlobalData.itemsOfYear[(int) spnYear1.getSelectedItemId()];
    	
    	return ERROR_NONE;
	}
}
