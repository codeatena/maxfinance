package com.morganfinance.submit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

import com.example.morganfinance.GlobalData;
import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

public class EmploymentCompanyDetail {

	Activity parent;
	public View view;
	
	public static final int ERROR_NONE = -1;
	public static final int ERROR_GENERAL = 0;
	public static final int ERROR_TITLE_IN_OCCUPATION = 1;
	public static final int ERROR_EMPLOYER_NAME = 2;
	public static final int ERROR_HR_CONTACT_NUMBER = 3;
	public static final int ERROR_WORK_INCOME_AFTER_TAX = 4;
	public static final int ERROR_CENTRELINK_PENSION = 5;
	public static final int ERROR_ADD_OTHER_INCOME = 6;

	Spinner spnEmploymentStatus;
	Spinner spnEmploymentType;
	
	LinearLayout		llytCenterlinkIncome;
	
	public EditText		edtTitleOccupation;
	public EditText		edtEmployerName;
	public EditText		edtContactNumber;
	
	public EditText		edtWokIncomAfterTax;
	Spinner			spnWokIncomAfterTax;

	public EditText		edtCentrelinkPension;
	
	LinearLayout	llytCentrelinkPension;
	
	Button			btnRemoveCentrelinkPension;
	Button			btnAddOtherIncome;
	
	LinearLayout	llytAddOtherIncome;
	Spinner			spnAddOtherIncome;
	public EditText		edtAddOtherIncome;
	Spinner			spnAddOtherIncomePeriodly;
	
	Button			btnRemoveOtherIncome;
	
	boolean 		isShowCenterLinkPension;
	boolean 		isAddOtherIncome;
	
	public EmploymentCompanyDetail(Context context) {
		// TODO Auto-generated constructor stub
		parent = (Activity) context;
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
    	LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.detail_layout, null);
    	
    	spnEmploymentStatus = (Spinner) view.findViewById(R.id.year_spinner1);
    	spnEmploymentType = (Spinner) view.findViewById(R.id.registered_owner_name_spinner1);

    	llytCenterlinkIncome = (LinearLayout) view.findViewById(R.id.centerlink_income_layout);
    	
    	edtTitleOccupation = (EditText) view.findViewById(R.id.title_occupation_editText);
    	edtEmployerName = (EditText) view.findViewById(R.id.employer_name_editText);
    	edtContactNumber = (EditText) view.findViewById(R.id.hr_contact_number_editText);

    	edtWokIncomAfterTax = (EditText) view.findViewById(R.id.brand_editText1);
    	spnWokIncomAfterTax = (Spinner) view.findViewById(R.id.work_income_after_tax_spinner);
    	
    	llytCentrelinkPension = (LinearLayout) view.findViewById(R.id.certrelink_pension_layout);
    	
    	edtCentrelinkPension = (EditText) view.findViewById(R.id.model_editText1);
    	
    	btnRemoveCentrelinkPension = (Button) view.findViewById(R.id.no_under_finance_button1);
    	btnAddOtherIncome = (Button)	view.findViewById(R.id.yes_under_finance_button1);
    	
    	llytAddOtherIncome = (LinearLayout) view.findViewById(R.id.add_other_income_layout);
    	
    	spnAddOtherIncome = (Spinner) view.findViewById(R.id.add_other_income_spinner);
    	edtAddOtherIncome = (EditText) view.findViewById(R.id.add_other_income_editText);
    	spnAddOtherIncomePeriodly = (Spinner) view.findViewById(R.id.add_other_income_period_spinner);
    	btnRemoveOtherIncome = (Button) view.findViewById(R.id.remove_other_income_button);
    	
    	llytAddOtherIncome.setVisibility(View.GONE);
	}
	
	private void initEvent() {
		btnRemoveCentrelinkPension.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
//				showGeneralAlert("", "Email is crucial for communication during the application process");
				isShowCenterLinkPension = false;
				llytCentrelinkPension.setVisibility(View.GONE);
			}
        });
		
		btnAddOtherIncome.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
//				showGeneralAlert("", "Email is crucial for communication during the application process");
				isAddOtherIncome = true;
				btnAddOtherIncome.setVisibility(View.GONE);
				llytAddOtherIncome.setVisibility(View.VISIBLE);
			}
        });
		
		btnRemoveOtherIncome.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
//				showGeneralAlert("", "Email is crucial for communication during the application process");
				btnAddOtherIncome.setVisibility(View.VISIBLE);
				llytAddOtherIncome.setVisibility(View.GONE);
				
				isAddOtherIncome = false;
			}
        });
		
		spnEmploymentStatus.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		        // your code here
		    	if (position == 2) {
		    		llytCenterlinkIncome.setVisibility(View.GONE);
		    	} else {
		    		llytCenterlinkIncome.setVisibility(View.VISIBLE);
		    	}
		    	
		    	if (position == 1 || position == 2) {
		    		btnRemoveCentrelinkPension.setVisibility(View.GONE);
		    	} else {
		    		btnRemoveCentrelinkPension.setVisibility(View.VISIBLE);
		    	}
		    }
		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		        // your code here
		    }
		});
	}
	
	private void initValue() {
		ArrayAdapter<String> spinnerArrayAdapter;
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfEmploymentStatus);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnEmploymentStatus.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfTypeEmployment);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnEmploymentType.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfPeriodly);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnWokIncomAfterTax.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfAddOtherIncome);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnAddOtherIncome.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfPeriodly);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnAddOtherIncomePeriodly.setAdapter(spinnerArrayAdapter);
		
		isShowCenterLinkPension = true;
		isAddOtherIncome 		= false;
		loadSavedPreferences();
	}
	
	public void loadSavedPreferences() {
    	
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	
    	 String str;
         int n;

         n = sharedPreferences.getInt("nEmploymentStatus", 0);
         spnEmploymentStatus.setSelection(n);
         n = sharedPreferences.getInt("nEmploymentType", 0);
         spnEmploymentType.setSelection(n);
         str = sharedPreferences.getString("strTitleOccupation", "");
         edtTitleOccupation.setText(str);
         str = sharedPreferences.getString("strEmployerName", "");
         edtEmployerName.setText(str);
         str = sharedPreferences.getString("strHrContactNumber1", "");
         edtContactNumber.setText(str);
     	
        str = sharedPreferences.getString("strIncomeAfterTax", "");
        edtWokIncomAfterTax.setText(str);
        n = sharedPreferences.getInt("nIncomeAfterTaxPeriod", 0);
        spnWokIncomAfterTax.setSelection(n);
        str = sharedPreferences.getString("strCenterLinkCom", "");
        edtCentrelinkPension.setText(str);
        
        n = sharedPreferences.getInt("nOtherIncomeSpecify", 0);
        spnAddOtherIncome.setSelection(n);
        str = sharedPreferences.getString("strRentalIncome", "");
        edtAddOtherIncome.setText(str);
        n = sharedPreferences.getInt("nAddOtherIncomePeriodly", 0);
        spnAddOtherIncomePeriodly.setSelection(n);
    }
    
    public void savePreferences() {
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	
   	 	editor.putInt("nEmploymentStatus", spnEmploymentStatus.getSelectedItemPosition());
   	 	editor.putInt("nEmploymentType", spnEmploymentType.getSelectedItemPosition());
   	 	editor.putString("strTitleOccupation", edtTitleOccupation.getText().toString());
   	 	editor.putString("strEmployerName", edtEmployerName.getText().toString());
   	 	editor.putString("strHrContactNumber1", edtContactNumber.getText().toString());
   	 	
   	 	editor.putString("strIncomeAfterTax", edtWokIncomAfterTax.getText().toString());
   	 	editor.putInt("nIncomeAfterTaxPeriod", spnWokIncomAfterTax.getSelectedItemPosition());
   	 	editor.putString("strCenterLinkCom", edtCentrelinkPension.getText().toString());
   	 	
   	 	editor.putInt("nOtherIncomeSpecify", spnAddOtherIncome.getSelectedItemPosition());
   	 	editor.putString("strRentalIncome", edtAddOtherIncome.getText().toString());
   	 	editor.putInt("nAddOtherIncomePeriodly", spnAddOtherIncomePeriodly.getSelectedItemPosition());

   	 	editor.commit();
    }
    
	public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(parent)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            }
         })
         .show();
    }
	
	public int checkInput() {
		
		String str;
		
		str = edtTitleOccupation.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Title in Occupation!");
    		edtTitleOccupation.requestFocus();
    		return ERROR_TITLE_IN_OCCUPATION;
    	}
    	str = edtEmployerName.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Employer Name!");
    		edtEmployerName.requestFocus();
    		return ERROR_EMPLOYER_NAME;
    	}
    	str = edtContactNumber.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input H.R. Contact Number!");

    		edtContactNumber.requestFocus();
    		return ERROR_HR_CONTACT_NUMBER;
    	}
    	str = edtWokIncomAfterTax.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input value in Work Income after Tax!");

    		edtWokIncomAfterTax.requestFocus();
    		return ERROR_WORK_INCOME_AFTER_TAX;
    	}
    	
    	int n = spnEmploymentStatus.getSelectedItemPosition();
    	if (n == 1 || n == 2) {
        	str = edtCentrelinkPension.getText().toString();
        	if (isShowCenterLinkPension && str.equals("")) {
        		showGeneralAlert("Input", "Please input Centrelink/Pension!");
        		edtCentrelinkPension.requestFocus();
        		return ERROR_CENTRELINK_PENSION;
        	}
    	}
    	str = GlobalData.itemsOfAddOtherIncome[(int)  spnAddOtherIncome.getSelectedItemId()];
    	if (isAddOtherIncome && str.equals("- Other Income -")) {
    		showGeneralAlert("Input", "Please select Other Income!");
    		return ERROR_GENERAL;
    	}
    	str = edtAddOtherIncome.getText().toString();
    	if (isAddOtherIncome && str.equals("")) {
    		showGeneralAlert("Input", "Please input Other Income!");
    		edtCentrelinkPension.requestFocus();
    		return ERROR_ADD_OTHER_INCOME;
    	}
    	
    	GlobalData.personalInfo.strEmploymentStatus = GlobalData.itemsOfEmploymentStatus[(int)  spnEmploymentStatus.getSelectedItemId()];
    	GlobalData.personalInfo.strEmploymentType = GlobalData.itemsOfTypeEmployment[(int)  spnEmploymentType.getSelectedItemId()];
    	GlobalData.personalInfo.strTitleOccupation = edtTitleOccupation.getText().toString();
    	GlobalData.personalInfo.strEmployerName = edtEmployerName.getText().toString();
    	GlobalData.personalInfo.strHrContactNumber1 = edtContactNumber.getText().toString();
    	GlobalData.personalInfo.strIncomeAfterTax = edtWokIncomAfterTax.getText().toString();
    	GlobalData.personalInfo.strIncomeAfterTaxPeriod = GlobalData.itemsOfPeriodly[(int)  spnWokIncomAfterTax.getSelectedItemId()];
    	GlobalData.personalInfo.strCenterLinkCom = edtCentrelinkPension.getText().toString();
    	
    	GlobalData.personalInfo.strOtherIncomeSpecify = GlobalData.itemsOfAddOtherIncome[(int)  spnAddOtherIncome.getSelectedItemId()];
    	GlobalData.personalInfo.strRentalIncome = edtAddOtherIncome.getText().toString();
    	GlobalData.personalInfo.strOtherIncomePeriod = GlobalData.itemsOfPeriodly[(int)  spnAddOtherIncomePeriodly.getSelectedItemId()];
//    	public String strOtherIncomeSpecify = "";
//    	public String strRentalIncome = "";
//    	public String strOtherIncomePeriod = "";
    	
    	return ERROR_NONE;
	}
}
