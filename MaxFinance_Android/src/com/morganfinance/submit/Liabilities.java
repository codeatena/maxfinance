package com.morganfinance.submit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.morganfinance.GlobalData;
import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

public class Liabilities {
	Activity parent;
	public View view;
	
	public static final int ERROR_NONE = -1;
	public static final int ERROR_GENERAL = 0;
	
	public static final int ERROR_BOARDING_RENTING_COST = 11;
	public static final int ERROR_OTHER_LIVING_EXPENSES = 1;
	
	public static final int ERROR_OWING_CREDIT_LIMIT1 = 2;
	public static final int ERROR_MONTHLY_PAYMENT1 = 3;
	public static final int ERROR_LENDER1 = 4;
	
	public static final int ERROR_OWING_CREDIT_LIMIT2 = 5;
	public static final int ERROR_MONTHLY_PAYMENT2 = 6;
	public static final int ERROR_LENDER2 = 7;
	
	public static final int ERROR_PERSONAL_OWING_CREDIT_LIMIT = 8;
	public static final int ERROR_PERSONAL_MONTHLY_PAYMENT = 9;
	public static final int ERROR_PERSONAL_LENDER = 10;
	
	public LinearLayout llytBoardingRentingCost;
	public EditText		edtBoardingRentingCost;
	public EditText		edtOtherLivingExpenses;
	
	Button				btnOtherLivingExpenses;
	
	public EditText		edtOwingCreditLimet1;
	public EditText		edtMonthlyPayment1;
	public EditText		edtLender1;
	
	public EditText		edtOwingCreditLimet2;
	public EditText		edtMonthlyPayment2;
	public EditText		edtLender2;
	
	//   personal
	public EditText		edtPersonalOwingCreditLimit;
	public EditText		edtPersonalMothlyPayment;
	public EditText		edtPersonalLender;
	
	public LinearLayout	llytMortgage1;
	public LinearLayout	llytMortgage2;

	public Liabilities(Context context) {
		// TODO Auto-generated constructor stub
		parent = (Activity) context;
		initWidget();
		initEvent();
		initValue();
	}
	
	private void initWidget() {
    	LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.liabilities_layout, null);
    	
    	llytBoardingRentingCost = (LinearLayout) view.findViewById(R.id.boarding_renting_layout);
    	edtBoardingRentingCost 	= (EditText) view.findViewById(R.id.partner_name_editText);
    	edtOtherLivingExpenses 	= (EditText) view.findViewById(R.id.current_value_editText1);
    	
    	edtOwingCreditLimet1 	= (EditText) view.findViewById(R.id.mortgage_owing_credit_limit_editText1);
    	edtMonthlyPayment1		= (EditText) view.findViewById(R.id.mortgage_monthly_payment_editText1);
    	edtLender1				= (EditText) view.findViewById(R.id.mortgage_lender_editText1);
    	
    	edtOwingCreditLimet2 	= (EditText) view.findViewById(R.id.mortgage_owing_credit_limit_editText2);
    	edtMonthlyPayment2		= (EditText) view.findViewById(R.id.mortgage_monthly_payment_editText2);
    	edtLender2				= (EditText) view.findViewById(R.id.mortgage_lender_editText2);
    	
    	edtPersonalOwingCreditLimit 	= (EditText) view.findViewById(R.id.personal_loan_owing_credit_editText);
    	edtPersonalMothlyPayment		= (EditText) view.findViewById(R.id.personal_monthly_payment_editText);
    	edtPersonalLender				= (EditText) view.findViewById(R.id.personal_lender_editText);
    	
    	llytMortgage1 			= (LinearLayout) view.findViewById(R.id.mortgage_layout1);
    	llytMortgage2 			= (LinearLayout) view.findViewById(R.id.mortgage_layout2);
    	
    	btnOtherLivingExpenses = (Button) view.findViewById(R.id.inf_other_living_expenses_button);
    	
    	if (GlobalData.personalInfo.strResidentialStatus == "I am renting" || 
    			GlobalData.personalInfo.strResidentialStatus == "I am boarding") {
    		
    		llytBoardingRentingCost.setVisibility(View.VISIBLE);
    	} else {
    		llytBoardingRentingCost.setVisibility(View.GONE);
    	}
	}

	private void initEvent() {
		btnOtherLivingExpenses.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				showGeneralAlert("Information", "Living Expenses for you and your dependants " +
						"(excluding rental/mortgage payment)\n" +
						"\t Utilities cost\n" +
						"\t Food (include take-away)\n" +
						"\t Health (e.g. Doctor, Medicines, Others)\n" +
						"\t Insurance (e.g. health, car insurance)\n" +
						"\t Education (e.g. School fees, childcare)\n" +
						"\t Transport\n" +
						"\t Clothing\n" +
						"\t Entertainment\n" +
						"To assist you with your calculation, you may refer to ASIC�s Money Smart Budget Planner."
				);
			}
        });
	}
	
	private void initValue() {
		loadSavedPreferences();
	}
	
	public void loadSavedPreferences() {
    	
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	
    	 String str;
     	
         str = sharedPreferences.getString("strBoardingRentingCost", "");
         edtBoardingRentingCost.setText(str);
         str = sharedPreferences.getString("strLivingExpensesPayment", "");
         edtOtherLivingExpenses.setText(str);
         
         str = sharedPreferences.getString("strOwingCreditLimit1", "");
         edtOwingCreditLimet1.setText(str);
         str = sharedPreferences.getString("strMothlyPayment1", "");
         edtMonthlyPayment1.setText(str);
         str = sharedPreferences.getString("strLender1", "");
         edtLender1.setText(str);
         
         str = sharedPreferences.getString("strOwingCreditLimit2", "");
         edtOwingCreditLimet2.setText(str);
         str = sharedPreferences.getString("strMothlyPayment2", "");
         edtMonthlyPayment2.setText(str);
         str = sharedPreferences.getString("strLender2", "");
         edtLender2.setText(str);
         
         str = sharedPreferences.getString("strPersonalLoanOwing", "");
         edtPersonalOwingCreditLimit.setText(str);
         str = sharedPreferences.getString("strPersonalLoanPayment", "");
         edtPersonalMothlyPayment.setText(str);
         str = sharedPreferences.getString("strPersonalLoanLender", "");
         edtPersonalLender.setText(str);
    }
    
    public void savePreferences() {
    	
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	
   	 	editor.putString("strBoardingRentingCost", edtBoardingRentingCost.getText().toString());
   	 	editor.putString("strLivingExpensesPayment", edtOtherLivingExpenses.getText().toString());

   	 	editor.putString("strOwingCreditLimit1", edtOwingCreditLimet1.getText().toString());
   	 	editor.putString("strMothlyPayment1", edtMonthlyPayment1.getText().toString());
   	 	editor.putString("strLender1", edtLender1.getText().toString());

   	 	editor.putString("strOwingCreditLimit2", edtOwingCreditLimet2.getText().toString());
   	 	editor.putString("strMothlyPayment2", edtMonthlyPayment2.getText().toString());
   	 	editor.putString("strLender2", edtLender2.getText().toString());
   	 	
   	 	editor.putString("strPersonalLoanOwing", edtPersonalOwingCreditLimit.getText().toString());
   	 	editor.putString("strPersonalLoanPayment", edtPersonalMothlyPayment.getText().toString());
   	 	editor.putString("strPersonalLoanLender", edtPersonalLender.getText().toString());
   	 	
   	 	editor.commit();
    }
	
	public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(parent)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            }
         })
         .show();
    }
    
	public int checkInput() {
		
		String str;
		
		if (GlobalData.personalInfo.strResidentialStatus == "I am renting" || 
    			GlobalData.personalInfo.strResidentialStatus == "I am boarding") {
    		
			str = edtBoardingRentingCost.getText().toString();
			if (str.equals("")) {
	    		showGeneralAlert("Input", "Please input Boarding/Renting Cost!");
	    		return ERROR_BOARDING_RENTING_COST;
			}
    	}
		str = edtOtherLivingExpenses.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Other Living Expenses!");
    		edtOtherLivingExpenses.requestFocus();
    		return ERROR_OTHER_LIVING_EXPENSES;
    	}
    	
		if (MotorVehicle1.strUnderFinance1 == "Yes") {
			str = edtOwingCreditLimet1.getText().toString();
	    	if (str.equals("")) {
	    		showGeneralAlert("Input", "Please input Owing/Credit Limit of Mortgage 1!");
	    		return ERROR_OWING_CREDIT_LIMIT1;
	    	}
	    	str = edtMonthlyPayment1.getText().toString();
	    	if (str.equals("")) {
	    		showGeneralAlert("Input", "Please input Monthly Payment of Mortgage 1!");
	    		return ERROR_MONTHLY_PAYMENT1;
	    	}
	    	str = edtLender1.getText().toString();
	    	if (str.equals("")) {
	    		showGeneralAlert("Input", "Please input lender of Mortgage 1!");
	    		return ERROR_LENDER1;
	    	}
		}
		
		if (MotorVehicle2.strUnderFinance2 == "Yes") {
			str = edtOwingCreditLimet2.getText().toString();
	    	if (str.equals("")) {
	    		showGeneralAlert("Input", "Please input Owing/Credit Limit of Mortgage 2!");
	    		return ERROR_OWING_CREDIT_LIMIT2;
	    	}
	    	str = edtMonthlyPayment2.getText().toString();
	    	if (str.equals("")) {
	    		showGeneralAlert("Input", "Please input Monthly Payment of Mortgage 2!");
	    		return ERROR_MONTHLY_PAYMENT2;
	    	}
	    	str = edtLender2.getText().toString();
	    	if (str.equals("")) {
	    		showGeneralAlert("Input", "Please input lender of Mortgage 2!");
	    		return ERROR_LENDER2;
	    	}
		}
        
		if (GlobalData.personalInfo.strResidentialStatus == "I am renting" || 
    			GlobalData.personalInfo.strResidentialStatus == "I am boarding") {
			
	    	GlobalData.personalInfo.strBoardingRentingCost = edtBoardingRentingCost.getText().toString();
		}
		
    	GlobalData.personalInfo.strLivingExpensesPayment = edtOtherLivingExpenses.getText().toString();
    	    	
    	if (MotorVehicle1.strUnderFinance1 == "Yes") {
			GlobalData.personalInfo.strOwingCreditLimit1 = edtOwingCreditLimet1.getText().toString();
			GlobalData.personalInfo.strMothlyPayment1 = edtMonthlyPayment1.getText().toString();
			GlobalData.personalInfo.strLender1 = edtLender1.getText().toString();
		} else {
			GlobalData.personalInfo.strOwingCreditLimit1 = "";
			GlobalData.personalInfo.strMothlyPayment1 = "";
			GlobalData.personalInfo.strLender1 = "";
		}
    	
     	if (MotorVehicle2.strUnderFinance2 == "Yes") {
			GlobalData.personalInfo.strOwingCreditLimit2 = edtOwingCreditLimet2.getText().toString();
			GlobalData.personalInfo.strMothlyPayment2 = edtMonthlyPayment2.getText().toString();
			GlobalData.personalInfo.strLender2 = edtLender2.getText().toString();
		} else {
			GlobalData.personalInfo.strOwingCreditLimit2 = "";
			GlobalData.personalInfo.strMothlyPayment2 = "";
			GlobalData.personalInfo.strLender2 = "";
		}
     	
    	return ERROR_NONE;
	}
	
	public void showMortGage() {
		if (MotorVehicle1.strUnderFinance1 == "Yes") {
			llytMortgage1.setVisibility(View.VISIBLE);
		} else {
			llytMortgage1.setVisibility(View.GONE);
		}
		if (MotorVehicle2.strUnderFinance2 == "Yes") {
			llytMortgage2.setVisibility(View.VISIBLE);
		} else {
			llytMortgage2.setVisibility(View.GONE);
		}
	}
	
	public int checkPersonalLoanInput() {
				
		String str;
		str = edtPersonalOwingCreditLimit.getText().toString();
    	if (str.equals("")) {
//    		showGeneralAlert("Input", "Please input Owing/Credit Limit of Personal Loan!");
    		return ERROR_PERSONAL_OWING_CREDIT_LIMIT;
    	}
    	str = edtPersonalMothlyPayment.getText().toString();
    	if (str.equals("")) {
//    		showGeneralAlert("Input", "Please input Monthly Payment of Personal Loan!");
    		return ERROR_PERSONAL_MONTHLY_PAYMENT;
    	}
    	str = edtPersonalLender.getText().toString();
    	if (str.equals("")) {
//    		showGeneralAlert("Input", "Please input lender of Personal Loan!");
    		return ERROR_PERSONAL_LENDER;
    	}
    	
    	GlobalData.personalInfo.strPersonalLoanOwing = edtPersonalOwingCreditLimit.getText().toString();
    	GlobalData.personalInfo.strPersonalLoanPayment = edtPersonalMothlyPayment.getText().toString();
    	GlobalData.personalInfo.strPersonalLoanLender = edtPersonalLender.getText().toString();
    	
    	return ERROR_NONE;
	}
}
