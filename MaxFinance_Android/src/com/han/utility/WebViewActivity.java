package com.han.utility;

import com.example.morganfinance.R;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebViewActivity extends Activity {
	
	WebView viewWeb;
	public static String webViewUrl;
	
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        initWidget();
        initValue();
        initEvent();
    }
    
    private void initWidget() {
    	viewWeb = (WebView) findViewById(R.id.webView1);
    }
    
    private void initValue() {
    	viewWeb = (WebView) findViewById(R.id.webView1);
//    	viewWeb.getSettings().setJavaScriptEnabled(true);
    	viewWeb.loadUrl(webViewUrl);
    }
    
    private void initEvent() {

    }
    
    public static void setUrl(String url) {
    	webViewUrl = url;
    }
}
