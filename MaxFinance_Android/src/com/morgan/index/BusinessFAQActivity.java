package com.morgan.index;

import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BusinessFAQActivity extends Activity {
	Button btnBack;
	TextView		txtViewFullSite;
	Button btnApplyNow;
	
	LinearLayout	llytWhatisPreApproval;
	LinearLayout	pageWhatisPreApproval;
	ImageView		imgWhatisPreApproval;
			
	LinearLayout	llytHowToApply;
	LinearLayout	pageHowToApply;
	ImageView		imgHowToApply;
	
	LinearLayout	llytAmIEligible;
	LinearLayout	pageAmIEligible;
	ImageView		imgAmIEligible;
	
	LinearLayout	llytHowMuchCanIBorrow;
	LinearLayout	pageHowMuchCanIBorrow;
	ImageView		imgHowMuchCanIBorrow;
	
//	LinearLayout	llytHowCanISpeedUp;
//	LinearLayout	pageHowCanISpeedUp;
//	ImageView		imgHowCanISpeedUp;
	
	LinearLayout	llytHowDoIMaximise;
	LinearLayout	pageHowDoIMaximise;
	ImageView		imgHowDoIMaximise;
	
	LinearLayout	llytWhatHappenAfterIApply;
	LinearLayout	pageWhatHappenAfterIApply;
	ImageView		imgWhatHappenAfterIApply;
	
	//// Interest Rates and Repayments
	LinearLayout	llytWhatAreTheLoanRepayments;
	LinearLayout	pageWhatAreTheLoanRepayments;
	ImageView		imgWhatAreTheLoanRepayments;
	
	LinearLayout	llytWhatIsTheInterestRate;
	LinearLayout	pageWhatIsTheInterestRate;
	ImageView		imgWhatIsTheInterestRate;
	
	LinearLayout	llytAreThereAnyFeels;
	LinearLayout	pageAreThereAnyFeels;
	ImageView		imgAreThereAnyFeels;
	
	LinearLayout	llytWillACreditCheckBeListed;
	LinearLayout	pageWillACreditCheckBeListed;
	ImageView		imgWillACreditCheckBeListed;
	
	//// About Morgan Finance Australia
	LinearLayout	llytWillMyPrivacyBeProtected;
	LinearLayout	pageWillMyPrivacyBeProtected;
	ImageView		imgWillMyPrivacyBeProtected;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.business_faqs);
        
        initWidget();
        initValue();
        initEvent();
    }
	    
    private void initWidget() {
    	btnBack = (Button) findViewById(R.id.back_index_button);
    	btnApplyNow = (Button) findViewById(R.id.apply_now_button);
    	txtViewFullSite = (TextView) findViewById(R.id.index_view_full_textView);
    	
    	llytWhatisPreApproval = (LinearLayout) findViewById(R.id.eligible_for_almost_everyone_layout);
    	pageWhatisPreApproval = (LinearLayout) findViewById(R.id.eligible_for_almost_everyone_page);
    	imgWhatisPreApproval = (ImageView) findViewById(R.id.eligible_for_almost_every_business_imageView);
    			
    	llytHowToApply = (LinearLayout) findViewById(R.id.interest_saving_features_layout);
    	pageHowToApply = (LinearLayout) findViewById(R.id.interest_saving_features_page);
    	imgHowToApply  = (ImageView) findViewById(R.id.interest_saving_features_imageView);
    	
    	llytAmIEligible = (LinearLayout) findViewById(R.id.am_i_eligible_layout);
    	pageAmIEligible = (LinearLayout) findViewById(R.id.am_i_eligible_page);
    	imgAmIEligible  = (ImageView) findViewById(R.id.am_i_eligible_imageView);
    	
    	llytHowMuchCanIBorrow = (LinearLayout) findViewById(R.id.how_much_can_i_borrow_layout);
    	pageHowMuchCanIBorrow = (LinearLayout) findViewById(R.id.how_much_can_i_borrow_page);
    	imgHowMuchCanIBorrow = (ImageView) findViewById(R.id.how_much_can_i_borrow_imageView);
    	
//    	llytHowCanISpeedUp = (LinearLayout) findViewById(R.id.how_can_i_speed_up_layout);
//    	pageHowCanISpeedUp = (LinearLayout) findViewById(R.id.how_can_i_speed_up_page);
//    	imgHowCanISpeedUp = (ImageView) findViewById(R.id.how_can_i_speed_up_imageView);
    	
    	llytHowDoIMaximise = (LinearLayout) findViewById(R.id.how_do_i_maximise_layout);
    	pageHowDoIMaximise = (LinearLayout) findViewById(R.id.how_do_i_maximise_page);
    	imgHowDoIMaximise = (ImageView) findViewById(R.id.how_do_i_maximise_imageView);
    	
    	llytWhatHappenAfterIApply = (LinearLayout) findViewById(R.id.this_loan_layout);
    	pageWhatHappenAfterIApply = (LinearLayout) findViewById(R.id.what_happen_after_i_apply_page);
    	imgWhatHappenAfterIApply = (ImageView) findViewById(R.id.this_loan_imageView);
    	
    	llytWhatAreTheLoanRepayments = (LinearLayout) findViewById(R.id.what_are_the_loan_repayments_layout);
    	pageWhatAreTheLoanRepayments = (LinearLayout) findViewById(R.id.what_are_the_loan_repayments_page);
    	imgWhatAreTheLoanRepayments = (ImageView) findViewById(R.id.what_are_the_loan_repayments_imageView);
    	
    	llytWhatIsTheInterestRate = (LinearLayout) findViewById(R.id.what_is_the_interest_rate_layout);
    	pageWhatIsTheInterestRate = (LinearLayout) findViewById(R.id.what_is_the_interest_rate_page);
    	imgWhatIsTheInterestRate = (ImageView) findViewById(R.id.what_is_the_interest_rate_imageView);
    	
    	llytAreThereAnyFeels = (LinearLayout) findViewById(R.id.are_there_any_fees_involved_layout);
    	pageAreThereAnyFeels = (LinearLayout) findViewById(R.id.are_there_any_fees_involved_page);
    	imgAreThereAnyFeels = (ImageView) findViewById(R.id.are_there_any_fees_involved_imageView);
    	
    	llytWillACreditCheckBeListed = (LinearLayout) findViewById(R.id.will_a_credit_check_be_listed_layout);
    	pageWillACreditCheckBeListed = (LinearLayout) findViewById(R.id.will_a_credit_check_be_listed_page);
    	imgWillACreditCheckBeListed = (ImageView) findViewById(R.id.will_a_credit_check_be_listed_imageView);
    	
    	llytWillMyPrivacyBeProtected = (LinearLayout) findViewById(R.id.will_my_privacy_be_protected_layout);
    	pageWillMyPrivacyBeProtected = (LinearLayout) findViewById(R.id.will_my_privacy_be_protected_page);
    	imgWillMyPrivacyBeProtected = (ImageView) findViewById(R.id.will_my_privacy_be_protected_imageView);
    }
	    
    private void initValue() {

    }
	
	private void initEvent() {
    	btnBack.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goToIndexPage();
			}
        });
    	
    	btnApplyNow.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
    	txtViewFullSite.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
				startActivity(browserIntent);
			}
    	});
    	
    	setApplicationApprovalEvent();
    	setInterestRatesAndRepayments();
    	
    	llytWillMyPrivacyBeProtected.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageWillMyPrivacyBeProtected.getVisibility() == View.VISIBLE) {
					pageWillMyPrivacyBeProtected.setVisibility(View.GONE);
					imgWillMyPrivacyBeProtected.setImageResource(R.drawable.plus_icon);
				} else {
					pageWillMyPrivacyBeProtected.setVisibility(View.VISIBLE);
					imgWillMyPrivacyBeProtected.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    }
	
	private void setApplicationApprovalEvent() {
    	llytWhatisPreApproval.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageWhatisPreApproval.getVisibility() == View.VISIBLE) {
					pageWhatisPreApproval.setVisibility(View.GONE);
					imgWhatisPreApproval.setImageResource(R.drawable.plus_icon);
				} else {
					allApplicationApprovalPageGone();
					pageWhatisPreApproval.setVisibility(View.VISIBLE);
					imgWhatisPreApproval.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytHowToApply.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageHowToApply.getVisibility() == View.VISIBLE) {
					pageHowToApply.setVisibility(View.GONE);
					imgHowToApply.setImageResource(R.drawable.plus_icon);
				} else {
					allApplicationApprovalPageGone();
					pageHowToApply.setVisibility(View.VISIBLE);
					imgHowToApply.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytAmIEligible.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageAmIEligible.getVisibility() == View.VISIBLE) {
					pageAmIEligible.setVisibility(View.GONE);
					imgAmIEligible.setImageResource(R.drawable.plus_icon);
				} else {
					allApplicationApprovalPageGone();
					pageAmIEligible.setVisibility(View.VISIBLE);
					imgAmIEligible.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytHowMuchCanIBorrow.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageHowMuchCanIBorrow.getVisibility() == View.VISIBLE) {
					pageHowMuchCanIBorrow.setVisibility(View.GONE);
					imgHowMuchCanIBorrow.setImageResource(R.drawable.plus_icon);
				} else {
					allApplicationApprovalPageGone();
					pageHowMuchCanIBorrow.setVisibility(View.VISIBLE);
					imgHowMuchCanIBorrow.setImageResource(R.drawable.minus_icon);
				}
			}
        });
//    	llytHowCanISpeedUp.setOnClickListener(new Button.OnClickListener() {    		
//			@Override
//			public void onClick(View v) {
//				if (pageHowCanISpeedUp.getVisibility() == View.VISIBLE) {
//					pageHowCanISpeedUp.setVisibility(View.GONE);
//					imgHowCanISpeedUp.setImageResource(R.drawable.plus_icon);
//				} else {
//					allApplicationApprovalPageGone();
//					pageHowCanISpeedUp.setVisibility(View.VISIBLE);
//					imgHowCanISpeedUp.setImageResource(R.drawable.minus_icon);
//				}
//			}
//        });
    	llytHowDoIMaximise.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageHowDoIMaximise.getVisibility() == View.VISIBLE) {
					pageHowDoIMaximise.setVisibility(View.GONE);
					imgHowDoIMaximise.setImageResource(R.drawable.plus_icon);
				} else {
					allApplicationApprovalPageGone();
					pageHowDoIMaximise.setVisibility(View.VISIBLE);
					imgHowDoIMaximise.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytWhatHappenAfterIApply.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageWhatHappenAfterIApply.getVisibility() == View.VISIBLE) {
					pageWhatHappenAfterIApply.setVisibility(View.GONE);
					imgWhatHappenAfterIApply.setImageResource(R.drawable.plus_icon);
				} else {
					allApplicationApprovalPageGone();
					pageWhatHappenAfterIApply.setVisibility(View.VISIBLE);
					imgWhatHappenAfterIApply.setImageResource(R.drawable.minus_icon);
				}
			}
        });
	}
	
	private void setInterestRatesAndRepayments() {
    	
    	llytWhatAreTheLoanRepayments.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageWhatAreTheLoanRepayments.getVisibility() == View.VISIBLE) {
					pageWhatAreTheLoanRepayments.setVisibility(View.GONE);
					imgWhatAreTheLoanRepayments.setImageResource(R.drawable.plus_icon);
				} else {
					allInterestRatesPageGone();
					pageWhatAreTheLoanRepayments.setVisibility(View.VISIBLE);
					imgWhatAreTheLoanRepayments.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytWhatIsTheInterestRate.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageWhatIsTheInterestRate.getVisibility() == View.VISIBLE) {
					pageWhatIsTheInterestRate.setVisibility(View.GONE);
					imgWhatIsTheInterestRate.setImageResource(R.drawable.plus_icon);
				} else {
					allInterestRatesPageGone();
					pageWhatIsTheInterestRate.setVisibility(View.VISIBLE);
					imgWhatIsTheInterestRate.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytAreThereAnyFeels.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageAreThereAnyFeels.getVisibility() == View.VISIBLE) {
					pageAreThereAnyFeels.setVisibility(View.GONE);
					imgAreThereAnyFeels.setImageResource(R.drawable.plus_icon);
				} else {
					allInterestRatesPageGone();
					pageAreThereAnyFeels.setVisibility(View.VISIBLE);
					imgAreThereAnyFeels.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytWillACreditCheckBeListed.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageWillACreditCheckBeListed.getVisibility() == View.VISIBLE) {
					pageWillACreditCheckBeListed.setVisibility(View.GONE);
					imgWillACreditCheckBeListed.setImageResource(R.drawable.plus_icon);
				} else {
					allInterestRatesPageGone();
					pageWillACreditCheckBeListed.setVisibility(View.VISIBLE);
					imgWillACreditCheckBeListed.setImageResource(R.drawable.minus_icon);
				}
			}
        });
	}
	
	private void allInterestRatesPageGone() {
		pageWhatAreTheLoanRepayments.setVisibility(View.GONE);
		pageWhatIsTheInterestRate.setVisibility(View.GONE);
		pageAreThereAnyFeels.setVisibility(View.GONE);
		pageWillACreditCheckBeListed.setVisibility(View.GONE);
		
		imgWhatAreTheLoanRepayments.setImageResource(R.drawable.plus_icon);
		imgWhatIsTheInterestRate.setImageResource(R.drawable.plus_icon);
		imgAreThereAnyFeels.setImageResource(R.drawable.plus_icon);
		imgWillACreditCheckBeListed.setImageResource(R.drawable.plus_icon);
	}
	
	private void allApplicationApprovalPageGone() {
		pageWhatisPreApproval.setVisibility(View.GONE);
		pageHowToApply.setVisibility(View.GONE);
		pageAmIEligible.setVisibility(View.GONE);
		pageHowMuchCanIBorrow.setVisibility(View.GONE);
//		pageHowCanISpeedUp.setVisibility(View.GONE);
		pageHowDoIMaximise.setVisibility(View.GONE);
		pageWhatHappenAfterIApply.setVisibility(View.GONE);
		
		imgWhatisPreApproval.setImageResource(R.drawable.plus_icon);
		imgHowToApply.setImageResource(R.drawable.plus_icon);
		imgAmIEligible.setImageResource(R.drawable.plus_icon);
		imgHowMuchCanIBorrow.setImageResource(R.drawable.plus_icon);
//		imgHowCanISpeedUp.setImageResource(R.drawable.plus_icon);
		imgHowDoIMaximise.setImageResource(R.drawable.plus_icon);
		imgWhatHappenAfterIApply.setImageResource(R.drawable.plus_icon);
	}
	
	private void goToIndexPage() {
		this.finish();
	}
	
	private void goHomeActivity() {
		startActivity(new Intent(this, HomeActivity.class));
	}
}
