package com.morgan.index;

import com.example.morganfinance.R;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class BusinessExtend {

	IndexActivity parent;
	public View view;
	
	LinearLayout llytCompareBusinessLoans;
	LinearLayout llytFastBusinessLoans;
	LinearLayout llytPremiumBusinessLoans;
	LinearLayout llytUnsecuredBusinessLoans;
	LinearLayout llytEasySteps3;
	LinearLayout llytBusinessFaqs;

	public BusinessExtend(Context context) {
		parent = (IndexActivity) context;
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
    	LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.business_layout, null);
    	
		llytCompareBusinessLoans = (LinearLayout) view.findViewById(R.id.compare_business_loans_button_layout);
		llytFastBusinessLoans = (LinearLayout) view.findViewById(R.id.fast_business_loans_button_layout);
		llytPremiumBusinessLoans = (LinearLayout) view.findViewById(R.id.premium_business_loans_button_layout);
		llytUnsecuredBusinessLoans = (LinearLayout) view.findViewById(R.id.unsecured_business_loans_button_layout);
		llytEasySteps3 = (LinearLayout) view.findViewById(R.id.business_easy_steps_button_layout);
		llytBusinessFaqs = (LinearLayout) view.findViewById(R.id.business_faqs_button_layout);
	}
	
	private void initValue() {
		
	}
	
	private void initEvent() {
		llytCompareBusinessLoans.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/compare-business-loans.html"));
//				parent.startActivity(browserIntent);
				
				parent.startActivity(new Intent(parent, CompareBusinessLoansActivity.class));
			}
        });
		
		llytFastBusinessLoans.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/fast-business-loans.html"));
//				parent.startActivity(browserIntent);
				
				parent.startActivity(new Intent(parent, FastBusinessLoansActivity.class));
			}
        });
		
		llytPremiumBusinessLoans.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/premium-business-loans.html"));
//				parent.startActivity(browserIntent);
				
				parent.startActivity(new Intent(parent, PremiunBusinessLoansActivity.class));
			}
        });
		
		llytUnsecuredBusinessLoans.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/unsecured-business-loans.html"));
//				parent.startActivity(browserIntent);
				
				parent.startActivity(new Intent(parent, UnsecuredBusinessLoansActivity.class));
			}
        });
		
		llytEasySteps3.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/business-process.html"));
//				parent.startActivity(browserIntent);
				
				parent.startActivity(new Intent(parent, ThreeEasyStepActivity.class));
			}
        });
		
		llytBusinessFaqs.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/business-faq.html"));
//				parent.startActivity(browserIntent);
				
				parent.startActivity(new Intent(parent, BusinessFAQActivity.class));
			}
        });
	}
}
