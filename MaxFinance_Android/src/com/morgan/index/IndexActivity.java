package com.morgan.index;

import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;
import com.example.morganfinance.SubmitActivity;
import com.han.utility.WebViewActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class IndexActivity extends Activity {
	
	LinearLayout llytGetYourMoneyNowButton;
	LinearLayout llytPersonalButton;
	LinearLayout llytBusinessButton;
	LinearLayout llytHowItWorksButton;
	LinearLayout llytAboutUsButton;
	LinearLayout llytContactUsButton;
	
	LinearLayout llytPersonalExtend;
	LinearLayout llytBusinessExtend;
	
	TextView		txtViewFullSite;
		
	PersonalExtend viewPersonalExtend;
	BusinessExtend viewBusinessExtend;
	
	ImageView		imgPersonalExtend;
	ImageView		imgBusinessExtend;

	boolean 		isShowPersonalExtend;
	boolean 		isShowBusinessExtend;
	
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        
        initWidget();
        initValue();
        initEvent();
    }
    
    private void initWidget() {
    	
    	llytGetYourMoneyNowButton = (LinearLayout) findViewById(R.id.get_your_money_now_button_layout);
    	llytPersonalButton = (LinearLayout) findViewById(R.id.personal_button_layout);
    	llytBusinessButton = (LinearLayout) findViewById(R.id.business_button_layout);
    	llytHowItWorksButton = (LinearLayout) findViewById(R.id.how_it_works_button_layout);
    	llytAboutUsButton = (LinearLayout) findViewById(R.id.about_us_button_layout);
    	llytContactUsButton = (LinearLayout) findViewById(R.id.contact_us_button_layout);
    	    	
    	llytPersonalExtend = (LinearLayout) findViewById(R.id.personal_extend_layout);
    	llytBusinessExtend = (LinearLayout) findViewById(R.id.business_extend_layout);
    	
    	imgPersonalExtend = (ImageView) findViewById(R.id.personal_extend_imageView);
    	imgBusinessExtend = (ImageView) findViewById(R.id.business_extend_imageView);
    	
    	txtViewFullSite = (TextView) findViewById(R.id.index_view_full_textView);
    	
    	viewPersonalExtend = new PersonalExtend(this);
    	viewBusinessExtend = new BusinessExtend(this);
    	
    	llytPersonalExtend.addView(viewPersonalExtend.view);
    	llytBusinessExtend.addView(viewBusinessExtend.view);
    	
    	imgPersonalExtend.setImageResource(R.drawable.plus_icon);
    	imgBusinessExtend.setImageResource(R.drawable.plus_icon);
    }
    
    private void initValue() {
    	isShowPersonalExtend = false;
    	isShowBusinessExtend = false;
    	
    	llytPersonalExtend.setVisibility(View.GONE);
    	llytBusinessExtend.setVisibility(View.GONE);
    }
    
    private void initEvent() {
    	llytGetYourMoneyNowButton.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
    	
    	llytPersonalButton.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				isShowPersonalExtend = !isShowPersonalExtend;
				
				if (isShowPersonalExtend) {
					llytPersonalExtend.setVisibility(View.VISIBLE);
					imgPersonalExtend.setImageResource(R.drawable.minus_icon);
				} else {
					llytPersonalExtend.setVisibility(View.GONE);
					imgPersonalExtend.setImageResource(R.drawable.plus_icon);
				}
				
				if (isShowPersonalExtend) {
					isShowBusinessExtend = false;
					llytBusinessExtend.setVisibility(View.GONE);
					imgBusinessExtend.setImageResource(R.drawable.plus_icon);
				}
			}
        });
    	
    	llytBusinessButton.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				isShowBusinessExtend = !isShowBusinessExtend;
				
				if (isShowBusinessExtend) {
					llytBusinessExtend.setVisibility(View.VISIBLE);
					imgBusinessExtend.setImageResource(R.drawable.minus_icon);
				} else {
					llytBusinessExtend.setVisibility(View.GONE);
					imgBusinessExtend.setImageResource(R.drawable.plus_icon);
				}
				
				if (isShowBusinessExtend) {
					isShowPersonalExtend = false;
					llytPersonalExtend.setVisibility(View.GONE);
					imgPersonalExtend.setImageResource(R.drawable.plus_icon);
				}
			}
        });
    	
    	llytHowItWorksButton.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goHowItWorksPage();
			}
        });
    	
    	llytAboutUsButton.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goAboutUsPage();
			}
        });
    	
    	llytContactUsButton.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goContactUsPage();
			}
        });
    	
    	txtViewFullSite.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
				startActivity(browserIntent);
			}
        });
    }
    
    private void goHowItWorksPage() {
//    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/howitworks_mobile.html"));
//		startActivity(browserIntent);
		this.startActivity(new Intent(this, HowItWorksActivity.class));
//    	WebViewActivity.setUrl("http://www.morganfinance.com.au/mobile/howitworks_mobile.html");
//    	this.startActivity(new Intent(this, WebViewActivity.class));
    	
    }
    
    private void goAboutUsPage() {
//    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/aboutus_mobile.html"));
//		startActivity(browserIntent);
    	this.startActivity(new Intent(this, AboutUsActivity.class));
//    	WebViewActivity.setUrl("http://www.morganfinance.com.au/mobile/aboutus_mobile.html");
//    	this.startActivity(new Intent(this, WebViewActivity.class));
    }
    
    private void goContactUsPage() {
//    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/contactus_mobile.html"));
//    	startActivity(browserIntent);
    	this.startActivity(new Intent(this, ContactUsActivity.class));
//    	WebViewActivity.setUrl("http://www.morganfinance.com.au/mobile/contactus_mobile.html");
//    	this.startActivity(new Intent(this, WebViewActivity.class));
    }
    
    private void goHomeActivity() {
		startActivity(new Intent(this, HomeActivity.class));
    }
    
}
