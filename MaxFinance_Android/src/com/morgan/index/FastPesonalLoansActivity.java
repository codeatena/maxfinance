package com.morgan.index;

import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FastPesonalLoansActivity extends Activity {
	
	LinearLayout	llytThisLoan;
	LinearLayout	pageThisLoan;
	ImageView		imgThisLoan;
	
	LinearLayout	llytEligible;
	LinearLayout	pageEligible;
	ImageView		imgEligible;
	
	LinearLayout	llytInterest;
	LinearLayout	pageInterest;
	ImageView		imgInterest;
	
	LinearLayout	llytFlexible;
	LinearLayout	pageFlexible;
	ImageView		imgFlexible;
	
	Button btnBack;
	Button btnApplyNow1;
	Button btnApplyNow2;
	
	TextView		txtViewFullSite;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fast_personal_loan);
        
        initWidget();
        initValue();
        initEvent();
    }
	
	private void initWidget() {
    	txtViewFullSite = (TextView) findViewById(R.id.index_view_full_textView);

    	btnBack = (Button) findViewById(R.id.back_index_button);
    	
    	btnApplyNow1 = (Button) findViewById(R.id.apply_now_button);
    	btnApplyNow2 = (Button) findViewById(R.id.apply_now_button2);

		llytThisLoan = (LinearLayout) findViewById(R.id.this_loan_layout);
		pageThisLoan = (LinearLayout) findViewById(R.id.this_loan_page);
		imgThisLoan = (ImageView) findViewById(R.id.this_loan_imageView);
		
		llytEligible = (LinearLayout) findViewById(R.id.eligible_for_almost_everyone_layout);
		pageEligible = (LinearLayout) findViewById(R.id.eligible_for_almost_everyone_page);
		imgEligible = (ImageView) findViewById(R.id.eligible_for_almost_every_business_imageView);
		
		llytInterest = (LinearLayout) findViewById(R.id.interest_saving_features_layout);
		pageInterest = (LinearLayout) findViewById(R.id.interest_saving_features_page);
		imgInterest = (ImageView) findViewById(R.id.interest_saving_features_imageView);
		
		llytFlexible = (LinearLayout) findViewById(R.id.flexible_repayments_layout);
		pageFlexible = (LinearLayout) findViewById(R.id.flexible_repayments_page);
		imgFlexible = (ImageView) findViewById(R.id.flexible_repayments_imageView);
		
		LayoutInflater inflater = this.getLayoutInflater();
		View view;
		
    	view = inflater.inflate(R.layout.this_loan_page, null);
    	pageThisLoan.addView(view);
    	
    	view = inflater.inflate(R.layout.eligible_for_almost_everyone, null);
    	pageEligible.addView(view);
    	
    	view = inflater.inflate(R.layout.interest_saving_features, null);
    	pageInterest.addView(view);
    	
    	view = inflater.inflate(R.layout.flexible_repayments, null);
    	pageFlexible.addView(view);
    	
    	pageThisLoan.setVisibility(View.GONE);
//		pageThisLoan.addView(ld);
    	allPageGone();
	}
	
	private void allPageGone() {
    	pageEligible.setVisibility(View.GONE);
    	pageInterest.setVisibility(View.GONE);
    	pageFlexible.setVisibility(View.GONE);
    	
		imgEligible.setImageResource(R.drawable.plus_icon);
		imgInterest.setImageResource(R.drawable.plus_icon);
		imgFlexible.setImageResource(R.drawable.plus_icon);
	}
	
	private void initValue() {
		btnApplyNow1.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
		btnApplyNow2.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
		
		llytThisLoan.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageThisLoan.getVisibility() == View.VISIBLE) {
					pageThisLoan.setVisibility(View.GONE);
					imgThisLoan.setImageResource(R.drawable.plus_icon);
				} else {
					pageThisLoan.setVisibility(View.VISIBLE);
					imgThisLoan.setImageResource(R.drawable.minus_icon);
				}
			}
        });
		llytEligible.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageEligible.getVisibility() == View.VISIBLE) {
					pageEligible.setVisibility(View.GONE);
					imgEligible.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pageEligible.setVisibility(View.VISIBLE);
					imgEligible.setImageResource(R.drawable.minus_icon);
				}
			}
        });
		llytInterest.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageInterest.getVisibility() == View.VISIBLE) {
					pageInterest.setVisibility(View.GONE);
					imgInterest.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pageInterest.setVisibility(View.VISIBLE);
					imgInterest.setImageResource(R.drawable.minus_icon);
				}
			}
        });
		llytFlexible.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageFlexible.getVisibility() == View.VISIBLE) {
					pageFlexible.setVisibility(View.GONE);
					imgFlexible.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pageFlexible.setVisibility(View.VISIBLE);
					imgFlexible.setImageResource(R.drawable.minus_icon);
				}
			}
        });
		txtViewFullSite.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
				startActivity(browserIntent);
			}
        });
	}
	
	private void initEvent() {
    	btnBack.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goToIndexPage();
			}
        });
	}
	
    private void goToIndexPage() {
    	this.finish();
    }
    
	private void goHomeActivity() {
		startActivity(new Intent(this, HomeActivity.class));
	}
}
