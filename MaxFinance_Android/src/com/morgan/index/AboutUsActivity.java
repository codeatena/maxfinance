package com.morgan.index;

import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AboutUsActivity extends Activity {
	
	Button btnBack;
	TextView		txtViewFullSite;
	TextView 		txtGetLoanNow;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        
        initWidget();
        initValue();
        initEvent();
    }
    
    private void initWidget() {
    	btnBack = (Button) findViewById(R.id.back_index_button);
    	
    	txtViewFullSite = (TextView) findViewById(R.id.index_view_full_textView);
    	txtGetLoanNow 	= (TextView) findViewById(R.id.get_loan_now_textView);
    }
    
    private void initValue() {

    }
    
    private void initEvent() {
    	btnBack.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goToIndexPage();
			}
        });
    	
    	txtViewFullSite.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
				startActivity(browserIntent);
			}
        });
    	
    	txtGetLoanNow.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
    }
    
    private void goToIndexPage() {
    	this.finish();
    }
    
    private void goHomeActivity() {
		startActivity(new Intent(this, HomeActivity.class));
    }
}
