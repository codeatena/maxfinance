package com.morgan.index;

import com.example.morganfinance.R;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class PersonalExtend {

	IndexActivity parent;
	public View view;
	
	LinearLayout llytFastPersonalLoans;
	LinearLayout llytBondLoans;
	LinearLayout llytEasySteps3;
	LinearLayout llytFrequentlyAskedQuestions;

	public PersonalExtend(Context context) {
		parent = (IndexActivity) context;

		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		
    	LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.personal_layout, null);
    	
		llytFastPersonalLoans = (LinearLayout) view.findViewById(R.id.fast_personal_loans_button_layout);
		llytBondLoans = (LinearLayout) view.findViewById(R.id.boand_loans_button_layout);
		llytEasySteps3 = (LinearLayout) view.findViewById(R.id.easy_steps3_button_layout);
		llytFrequentlyAskedQuestions = (LinearLayout) view.findViewById(R.id.frequently_asked_questions_button_layout);
	}
	
	private void initValue() {
		
	}
	
	private void initEvent() {
		llytFastPersonalLoans.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/fast-personal-loans.html"));
				parent.startActivity(new Intent(parent, FastPesonalLoansActivity.class));
			}
        });
		
		llytBondLoans.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/bond-loans.html"));
//				parent.startActivity(browserIntent);
				parent.startActivity(new Intent(parent, BondLoansActivity.class));
			}
        });
		
		llytEasySteps3.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/personal-process.html"));
				parent.startActivity(new Intent(parent, ThreeEasyStepActivity.class));
			}
        });
		
		llytFrequentlyAskedQuestions.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
//				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile/personal-faq.html"));
//				parent.startActivity(browserIntent);
				parent.startActivity(new Intent(parent, FAQActivity.class));
			}
        });
	}
}
