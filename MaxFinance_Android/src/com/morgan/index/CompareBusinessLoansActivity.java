package com.morgan.index;

import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CompareBusinessLoansActivity extends Activity {

	Button btnBack;
	TextView		txtViewFullSite;
	Button btnApplyNow;
	
	LinearLayout llytFastBusinessLoans;
	LinearLayout llytPremiumBusinessLoans;
	LinearLayout llytFastBusinessLoansLater;
	
	LinearLayout pageFastBusinessLoans;
	LinearLayout pagePremiumBusinessLoans;
	LinearLayout pageFastBusinessLoansLater;
	
	ImageView imgFastBusinessLoans;
	ImageView imgPremiumBusinessLoans;
	ImageView imgFastBusinessLoansLater;
	
	Button	btnFastBusinessLoans;
	Button	btnPremiumBusinessLoans;
	Button	btnFastBusinessLoansLater;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.compare_business_loans);
		
		initWidget();
		initValue();
		initEvent();
	}
	    
    private void initWidget() {
    	btnBack = (Button) findViewById(R.id.back_index_button);
    	btnApplyNow = (Button) findViewById(R.id.apply_now_button2);
    	txtViewFullSite = (TextView) findViewById(R.id.index_view_full_textView);
    	
    	llytFastBusinessLoans = (LinearLayout) findViewById(R.id.eligible_for_almost_everyone_layout);
    	llytPremiumBusinessLoans = (LinearLayout) findViewById(R.id.interest_saving_features_layout);
    	llytFastBusinessLoansLater = (LinearLayout) findViewById(R.id.flexible_repayments_layout);
    	
    	pageFastBusinessLoans = (LinearLayout) findViewById(R.id.eligible_for_almost_everyone_page);
    	pagePremiumBusinessLoans = (LinearLayout) findViewById(R.id.interest_saving_features_page);
    	pageFastBusinessLoansLater = (LinearLayout) findViewById(R.id.flexible_repayments_page);
    	
    	imgFastBusinessLoans = (ImageView) findViewById(R.id.eligible_for_almost_every_business_imageView);
    	imgPremiumBusinessLoans = (ImageView) findViewById(R.id.interest_saving_features_imageView);
    	imgFastBusinessLoansLater = (ImageView) findViewById(R.id.flexible_repayments_imageView);
    	
    	btnFastBusinessLoans = (Button) findViewById(R.id.fast_business_loans_button);
    	btnPremiumBusinessLoans = (Button) findViewById(R.id.premium_business_loans_button);
    	btnFastBusinessLoansLater = (Button) findViewById(R.id.fast_business_loans_later_button);
    }
    
    private void initValue() {

    }
	    
    private void initEvent() {
    	btnBack.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goToIndexPage();
			}
        });
    	
    	btnApplyNow.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
    	txtViewFullSite.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
				startActivity(browserIntent);
			}
    	});
    	llytFastBusinessLoans.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageFastBusinessLoans.getVisibility() == View.VISIBLE) {
					pageFastBusinessLoans.setVisibility(View.GONE);
					imgFastBusinessLoans.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pageFastBusinessLoans.setVisibility(View.VISIBLE);
					imgFastBusinessLoans.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytPremiumBusinessLoans.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pagePremiumBusinessLoans.getVisibility() == View.VISIBLE) {
					pagePremiumBusinessLoans.setVisibility(View.GONE);
					imgPremiumBusinessLoans.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pagePremiumBusinessLoans.setVisibility(View.VISIBLE);
					imgPremiumBusinessLoans.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	llytFastBusinessLoansLater.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageFastBusinessLoansLater.getVisibility() == View.VISIBLE) {
					pageFastBusinessLoansLater.setVisibility(View.GONE);
					imgFastBusinessLoansLater.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pageFastBusinessLoansLater.setVisibility(View.VISIBLE);
					imgFastBusinessLoansLater.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	btnFastBusinessLoans.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
    	btnPremiumBusinessLoans.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
    	btnFastBusinessLoansLater.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
    }
	
	private void goToIndexPage() {
		this.finish();
	}
	
	private void goHomeActivity() {
		startActivity(new Intent(this, HomeActivity.class));
	}
	
	private void allPageGone() {
		pageFastBusinessLoans.setVisibility(View.GONE);
		pagePremiumBusinessLoans.setVisibility(View.GONE);
		pageFastBusinessLoansLater.setVisibility(View.GONE);
    	
		imgFastBusinessLoans.setImageResource(R.drawable.plus_icon);
		imgPremiumBusinessLoans.setImageResource(R.drawable.plus_icon);
		imgFastBusinessLoansLater.setImageResource(R.drawable.plus_icon);
	}
}
