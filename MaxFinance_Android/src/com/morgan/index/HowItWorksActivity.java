package com.morgan.index;

import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HowItWorksActivity extends Activity {
	
	Button btnBack;
	Button btnApplyNow;
	TextView		txtViewFullSite;

	
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.how_it_works_layout);
        
        initWidget();
        initValue();
        initEvent();
    }
    
    private void initWidget() {
    	btnBack = (Button) findViewById(R.id.back_index_button);
    	btnApplyNow = (Button) findViewById(R.id.apply_now_button);
    	txtViewFullSite = (TextView) findViewById(R.id.how_view_full_textView);
    }
    
    private void initValue() {
    	btnBack.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goToIndexPage();
			}
        });
    	
    	btnApplyNow.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goHomeActivity();			
			}
        });
    	
    	txtViewFullSite.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
				startActivity(browserIntent);
			}
        });
    }
    
    private void initEvent() {
    	
    }
    
    private void goToIndexPage() {
    	this.finish();
    }
    
    private void goHomeActivity() {
		startActivity(new Intent(this, HomeActivity.class));
    }
}
