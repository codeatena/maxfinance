package com.morgan.index;

import com.example.morganfinance.GlobalData;
import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ContactUsActivity extends Activity {
	
	private static final int REQUEST_CODE_CONTACTED_SHORTLY = 1000;
	
	Button btnBack;
	TextView		txtViewFullSite;
	
	Spinner spnPurpose;
	EditText edtMessage;
	EditText edtFullName;
	EditText edtPhone;
	EditText editEmail;
	
	Button btnSubmit;

	 protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_contact_us);
	        
	        initWidget();
	        initValue();
	        initEvent();
	    }
	    
	    private void initWidget() {
	    	btnBack = (Button) findViewById(R.id.back_index_button);
	    	txtViewFullSite = (TextView) findViewById(R.id.how_view_full_textView);
	    	
	    	spnPurpose = (Spinner) findViewById(R.id.purpose_spinner);
	    	edtMessage = (EditText) findViewById(R.id.message_editText);
	    	edtFullName = (EditText) findViewById(R.id.full_name_editText);
	    	edtPhone = (EditText) findViewById(R.id.phone_editText);
	    	editEmail = (EditText) findViewById(R.id.contact_email_editText);
	    	
	    	btnSubmit = (Button) findViewById(R.id.contact_submit_button);
	    }
	    
	    private void initValue() {
			ArrayAdapter<String> spinnerArrayAdapter;
			
			spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, GlobalData.itemsOfPurpose);
			spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
			spnPurpose.setAdapter(spinnerArrayAdapter);
	    }
	    
	    private void initEvent() {
	    	btnBack.setOnClickListener(new Button.OnClickListener() {
	    		
				@Override
				public void onClick(View v) {
					goToIndexPage();
				}
	        });
	    	
	    	txtViewFullSite.setOnClickListener(new Button.OnClickListener() {
	    		
				@Override
				public void onClick(View v) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
					startActivity(browserIntent);
				}
	        });
	    	
	    	btnSubmit.setOnClickListener(new Button.OnClickListener() {
	    		
				@Override
				public void onClick(View v) {
					submit();
				}
	        });
	    }
	    
	    private void goToIndexPage() {
	    	this.finish();
	    }
	    
	    private void submit() {
	    	if (!checkInput()) return;
	    	this.startActivityForResult(new Intent(this, ContactedShortlyActivity.class), REQUEST_CODE_CONTACTED_SHORTLY);
	    }
	    
	    private boolean checkInput() {
	    	String str;
	    	
	    	str = GlobalData.itemsOfPurpose[(int) spnPurpose.getSelectedItemPosition()];
	    	if (str == "-Select-") {
	    		spnPurpose.requestFocus();
	    		showGeneralAlert("Select", "Please select a Purpose!");
	    		return false;
	    	}
	    	str = edtMessage.getText().toString();
	    	if (str.equals("")) {
	    		edtMessage.requestFocus();
	    		showGeneralAlert("Input", "Please input Message!");
	    		return false;
	    	}
	    	str = edtFullName.getText().toString();
	    	if (str.equals("")) {
	    		edtFullName.requestFocus();
	    		showGeneralAlert("Input", "Please input Full Name!");
	    		return false;
	    	}
	    	str = edtPhone.getText().toString();
	    	if (str.equals("")) {
	    		edtPhone.requestFocus();
	    		showGeneralAlert("Input", "Please input Phone number!");
	    		return false;
	    	}
	    	return true;
	    }
	    
	    
	    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
	    	if (requestCode == REQUEST_CODE_CONTACTED_SHORTLY) {
	    		finish();
	    	}
	    }	    	
	    
	    public void showGeneralAlert(String title, String message) {
	    	new AlertDialog.Builder(ContactUsActivity.this)
	        .setTitle(title)
	        .setMessage(message)
	        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) { 
	                // continue with delete
	            }
	         })
	         .show();
	    }
}