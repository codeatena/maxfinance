package com.morgan.index;

import com.example.morganfinance.HomeActivity;
import com.example.morganfinance.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PremiunBusinessLoansActivity extends Activity {
	
	Button btnApplyNow1;
	Button btnApplyNow2;
	
	LinearLayout	llytThisLoan;
	LinearLayout	pageThisLoan;
	ImageView		imgThisLoan;
	
	LinearLayout	llytEligible;
	LinearLayout	pageEligible;
	ImageView		imgEligible;
	
	LinearLayout	llytInterest;
	LinearLayout	pageInterest;
	ImageView		imgInterest;
	
	LinearLayout	llytFlexible;
	LinearLayout	pageFlexible;
	ImageView		imgFlexible;
	
	Button btnBack;
	TextView		txtViewFullSite;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.premium_business_loan);
        
        initWidget();
        initValue();
        initEvent();
    }
	
	private void initWidget() {
    	btnBack = (Button) findViewById(R.id.back_index_button);

    	btnApplyNow1 = (Button) findViewById(R.id.apply_now_button);
    	btnApplyNow2 = (Button) findViewById(R.id.apply_now_button2);
    	
		llytThisLoan = (LinearLayout) findViewById(R.id.this_loan_layout);
		pageThisLoan = (LinearLayout) findViewById(R.id.this_loan_page);
		imgThisLoan = (ImageView) findViewById(R.id.this_loan_imageView);
		
		llytEligible = (LinearLayout) findViewById(R.id.eligible_for_almost_everyone_layout);
		pageEligible = (LinearLayout) findViewById(R.id.eligible_for_almost_everyone_page);
		imgEligible = (ImageView) findViewById(R.id.eligible_for_almost_every_business_imageView);
		
		llytInterest = (LinearLayout) findViewById(R.id.interest_saving_features_layout);
		pageInterest = (LinearLayout) findViewById(R.id.interest_saving_features_page);
		imgInterest = (ImageView) findViewById(R.id.interest_saving_features_imageView);
		
		llytFlexible = (LinearLayout) findViewById(R.id.flexible_repayments_layout);
		pageFlexible = (LinearLayout) findViewById(R.id.flexible_repayments_page);
		imgFlexible = (ImageView) findViewById(R.id.flexible_repayments_imageView);
	
    	pageThisLoan.setVisibility(View.GONE);
    	txtViewFullSite = (TextView) findViewById(R.id.index_view_full_textView);

    	allPageGone();
	}
	
	private void allPageGone() {
    	pageEligible.setVisibility(View.GONE);
    	pageInterest.setVisibility(View.GONE);
    	pageFlexible.setVisibility(View.GONE);
    	
		imgEligible.setImageResource(R.drawable.plus_icon);
		imgInterest.setImageResource(R.drawable.plus_icon);
		imgFlexible.setImageResource(R.drawable.plus_icon);
	}
	
	private void initValue() {
		btnApplyNow1.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
		btnApplyNow2.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				goHomeActivity();
			}
        });
		
		llytThisLoan.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageThisLoan.getVisibility() == View.VISIBLE) {
					pageThisLoan.setVisibility(View.GONE);
					imgThisLoan.setImageResource(R.drawable.plus_icon);
				} else {
					pageThisLoan.setVisibility(View.VISIBLE);
					imgThisLoan.setImageResource(R.drawable.minus_icon);
				}
			}
        });
		llytEligible.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageEligible.getVisibility() == View.VISIBLE) {
					pageEligible.setVisibility(View.GONE);
					imgEligible.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pageEligible.setVisibility(View.VISIBLE);
					imgEligible.setImageResource(R.drawable.minus_icon);
				}
			}
        });
		llytInterest.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageInterest.getVisibility() == View.VISIBLE) {
					pageInterest.setVisibility(View.GONE);
					imgInterest.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pageInterest.setVisibility(View.VISIBLE);
					imgInterest.setImageResource(R.drawable.minus_icon);
				}
			}
        });
		llytFlexible.setOnClickListener(new Button.OnClickListener() {    		
			@Override
			public void onClick(View v) {
				if (pageFlexible.getVisibility() == View.VISIBLE) {
					pageFlexible.setVisibility(View.GONE);
					imgFlexible.setImageResource(R.drawable.plus_icon);
				} else {
					allPageGone();
					pageFlexible.setVisibility(View.VISIBLE);
					imgFlexible.setImageResource(R.drawable.minus_icon);
				}
			}
        });
    	txtViewFullSite.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
				startActivity(browserIntent);
			}
        });
	}
	
	private void initEvent() {
    	btnBack.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				goToIndexPage();
			}
        });
	}
	
    private void goToIndexPage() {
    	this.finish();
    }
    
	private void goHomeActivity() {
		startActivity(new Intent(this, HomeActivity.class));
	}
}
