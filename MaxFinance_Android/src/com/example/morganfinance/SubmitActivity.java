package com.example.morganfinance;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.view.View;

import com.morganfinance.submit.Asset;
import com.morganfinance.submit.CreditHistory;
import com.morganfinance.submit.EmploymentCompanyDetail;
import com.morganfinance.submit.Liabilities;
import com.morganfinance.submit.MotorVehicle1;
import com.morganfinance.submit.MotorVehicle2;
import com.morganfinance.submit.RealEstate1;
import com.morganfinance.submit.RealEstate2;

public class SubmitActivity extends Activity implements View.OnClickListener {
	
	final static int VIEW_ASSET 			= 0;
	final static int VIEW_MOTOR_VEHICLE 	= 1;
	final static int VIEW_REAL_ESTATE 		= 2;
	final static int VIEW_LIABILITIES 		= 3;
	final static int VIEW_DETAIL 			= 4;
	final static int VIEW_CREDIT_HISTORY 	= 5;

	LinearLayout 	llytAsset;
	
	LinearLayout 	llytMotorVehicle1;
	LinearLayout 	llytMotorVehicle2;
	
	LinearLayout 	llytRealEstate1;
	LinearLayout 	llytRealEstate2;
	
	LinearLayout 	llytLiabilities;
	LinearLayout 	llytDetail;
	LinearLayout 	llytCreditHistory;
	
	Button 			btnAsset;
	public Button 			btnMotorVehicle;
	public Button			btnRealEstate;
	Button 			btnLiabilities;
	Button 			btnDetail;
	Button 			btnCreditHistory;
	
	Asset 			viewAsset;
	MotorVehicle1 	viewMotorVehicle1;
	MotorVehicle2 	viewMotorVehicle2;
	RealEstate1		viewRealEstate1;
	RealEstate2		viewRealEstate2;
	Liabilities		viewLiabilities;
	EmploymentCompanyDetail viewDetail;
	CreditHistory	viewCreditHistory;
	
	Button 			btnNeedHelp;
	Button 			btnGoSite;
	Button 			btnBack;
	Button 			btnSubmit;
	
	int				m_nSelectView;

	int 			nErrorCode;
//    public static ProgressDialog dlgLoading;

    boolean			m_isHasPersonalLoan;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);
        
        GlobalData.makeItemsOfRegisteredOwnersNameForMortorVehicle();
        GlobalData.makeItemsOfRegisteredOwnersNameForRealEstate();
        
        initWidget();
        initEvent();
        initValue();
    }
    
    private void initValue() {
    	m_isHasPersonalLoan = true;
    	m_nSelectView = VIEW_ASSET;
    	selectView(m_nSelectView);
    }
    
    private void savePreferences() {
    	viewAsset.savePreferences();
    	viewMotorVehicle1.savePreferences();
    	viewMotorVehicle2.savePreferences();
    	viewRealEstate1.savePreferences();
    	viewRealEstate1.savePreferences();
    	viewLiabilities.savePreferences();
    	viewDetail.savePreferences();
    	viewCreditHistory.savePreferences();
    }
    
    private void selectView(int nView) {
    	
    	llytAsset.setVisibility(View.GONE);
    	llytMotorVehicle1.setVisibility(View.GONE);
    	llytMotorVehicle2.setVisibility(View.GONE);
    	llytRealEstate1.setVisibility(View.GONE);
    	llytRealEstate2.setVisibility(View.GONE);
    	llytLiabilities.setVisibility(View.GONE);
    	llytDetail.setVisibility(View.GONE);
    	llytCreditHistory.setVisibility(View.GONE);
    	
    	savePreferences();
    	
    	m_nSelectView = nView;
    	if (m_nSelectView == VIEW_ASSET) {
    		llytAsset.setVisibility(View.VISIBLE);
    	}
    	if (m_nSelectView == VIEW_MOTOR_VEHICLE) {
    		
    		if (GlobalData.personalInfo.countMotorVehicle > 0) {
    			llytMotorVehicle1.setVisibility(View.VISIBLE);
    		}
    		if (GlobalData.personalInfo.countMotorVehicle > 1) {
    			llytMotorVehicle2.setVisibility(View.VISIBLE);
    		}
    	}
    	if (m_nSelectView == VIEW_REAL_ESTATE) {
    		
    		if (GlobalData.personalInfo.countRealState > 0) {
    			llytRealEstate1.setVisibility(View.VISIBLE);
    		}
    		if (GlobalData.personalInfo.countRealState > 1) {
    			llytRealEstate2.setVisibility(View.VISIBLE);	
    		}
    	}
    	if (m_nSelectView == VIEW_LIABILITIES) {
    		viewLiabilities.showMortGage();
    		llytLiabilities.setVisibility(View.VISIBLE);
    	}
    	if (m_nSelectView == VIEW_DETAIL) {
    		llytDetail.setVisibility(View.VISIBLE);
    	}
    	if (m_nSelectView == VIEW_CREDIT_HISTORY) {
    		llytCreditHistory.setVisibility(View.VISIBLE);
    	}
    }
    
    private void initWidget() {
    	llytAsset 			= (LinearLayout) findViewById(R.id.asset_layout);
    	
    	llytMotorVehicle1 	= (LinearLayout) findViewById(R.id.motor_vehicle_layout1);
    	llytMotorVehicle2 	= (LinearLayout) findViewById(R.id.motor_vehicle_layout2);
    	
    	llytRealEstate1 	= (LinearLayout) findViewById(R.id.real_estate_layout1);
    	llytRealEstate2 	= (LinearLayout) findViewById(R.id.real_estate_layout2);
    	
    	llytLiabilities 	= (LinearLayout) findViewById(R.id.liabilities_layout);
    	llytDetail 			= (LinearLayout) findViewById(R.id.detail_layout);
    	llytCreditHistory 	= (LinearLayout) findViewById(R.id.credit_history_layout);
    	
    	btnAsset 			= (Button) findViewById(R.id.no_under_finance_button1);
    	btnMotorVehicle 	= (Button) findViewById(R.id.yes_under_finance_button1);
    	btnRealEstate		= (Button) findViewById(R.id.real_estate_button);
    	btnLiabilities 		= (Button) findViewById(R.id.liabilities_button);
    	btnDetail 			= (Button) findViewById(R.id.inf_under_finance_button1);
    	btnCreditHistory 	= (Button) findViewById(R.id.credit_history_button);
    	
    	btnNeedHelp			= (Button) findViewById(R.id.need_help_button);
    	btnGoSite 			= (Button) findViewById(R.id.go_site_button);
    	
    	btnBack				= (Button) findViewById(R.id.inf_brand_button1);
    	btnSubmit			= (Button) findViewById(R.id.submit_button_submit);
    	
		viewAsset 			= new Asset(this);
		
		viewMotorVehicle1 	= new MotorVehicle1(this);
		viewMotorVehicle2 	= new MotorVehicle2(this);
		
		viewRealEstate1 	= new RealEstate1(this);
		viewRealEstate2 	= new RealEstate2(this);
		
		viewLiabilities		= new Liabilities(this);
		viewDetail  		= new EmploymentCompanyDetail(this);
		viewCreditHistory	= new CreditHistory(this);
		
		llytAsset.addView(viewAsset.view);
		
		llytMotorVehicle1.addView(viewMotorVehicle1.view);
		llytMotorVehicle2.addView(viewMotorVehicle2.view);
		
		llytRealEstate1.addView(viewRealEstate1.view);
		llytRealEstate2.addView(viewRealEstate2.view);

		llytLiabilities.addView(viewLiabilities.view);
		llytDetail.addView(viewDetail.view);
		llytCreditHistory.addView(viewCreditHistory.view);
    }
    
    private void initEvent() {
    	
    	btnNeedHelp.setOnClickListener(new Button.OnClickListener() {
    		
			@Override
			public void onClick(View v) {
				final AlertDialog.Builder alert = new AlertDialog.Builder(SubmitActivity.this);
			    final EditText input = new EditText(SubmitActivity.this);
			    alert.setTitle("What issues are you experiencing?");
			    alert.setView(input);
			    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int whichButton) {
//			            dialog.cancel();
			        }
			    });
			    alert.setPositiveButton("Talk to Us", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int whichButton) {

			        	Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
			        	email.putExtra(Intent.EXTRA_EMAIL, new String[] {"victor@morganfinance.com.au"});
			            email.putExtra(Intent.EXTRA_SUBJECT, "Mobile App technical error report");
			            email.putExtra(Intent.EXTRA_TEXT, input.getText().toString());
			        	email.setType("text/plain");			             

			            startActivity(Intent.createChooser(email, "E-mail"));
			        }
			    });
			    
			    alert.show();
			}
        });
    	
    	btnBack.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
        });
    	
    	btnSubmit.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				savePreferences();
//				if (!viewAsset.checkInput()) {
//					selectView(VIEW_ASSET);
//					return;
//				}
				
				if (!checkMotorVehicle()) return;
//				goCompletePage();
//				sendDataProc();
			}
        });
    	
    	btnGoSite.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.morganfinance.com.au/mobile_index.php"));
				startActivity(browserIntent);
			}
        });
    	
    	btnAsset.setOnClickListener(this);
    	btnMotorVehicle.setOnClickListener(this);
    	btnRealEstate.setOnClickListener(this);
    	btnLiabilities.setOnClickListener(this);
    	btnDetail.setOnClickListener(this);
    	btnCreditHistory.setOnClickListener(this);
    }
    
    public void goCompletePage() {
    	startActivity(new Intent(this, CompleteActivity.class));
    }
    
    public boolean checkMotorVehicle() {
    	if (GlobalData.personalInfo.countMotorVehicle > 0) {
			
			nErrorCode = viewMotorVehicle1.checkInput();
			if (nErrorCode != MotorVehicle1.ERROR_NONE) {
				selectView(VIEW_MOTOR_VEHICLE);
				
				if (nErrorCode == MotorVehicle1.ERROR_CURRENT_VALUE1)
					viewMotorVehicle1.edtCurrentValue1.requestFocus();
				if (nErrorCode == MotorVehicle1.ERROR_BRAND1)
					viewMotorVehicle1.edtBrand1.requestFocus();
				if (nErrorCode == MotorVehicle1.ERROR_MODEL1)
					viewMotorVehicle1.edtModel1.requestFocus();
				
				return false;
			}
		}
		
		if (GlobalData.personalInfo.countMotorVehicle > 1) {
			
			nErrorCode = viewMotorVehicle2.checkInput();
			if (nErrorCode != MotorVehicle2.ERROR_NONE) {
				selectView(VIEW_MOTOR_VEHICLE);
				
				if (nErrorCode == MotorVehicle2.ERROR_CURRENT_VALUE2)
					viewMotorVehicle2.edtCurrentValue2.requestFocus();
				if (nErrorCode == MotorVehicle2.ERROR_BRAND2)
					viewMotorVehicle2.edtBrand2.requestFocus();
				if (nErrorCode == MotorVehicle2.ERROR_MODEL2)
					viewMotorVehicle2.edtModel2.requestFocus();
				
				return false;
			}
		}
		
		return checkRealEstate();
    }
    
    public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(SubmitActivity.this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            }
         })
         .show();
    }
    
    public boolean checkRealEstate() {
    	if (GlobalData.personalInfo.countRealState > 0) {
			
			nErrorCode = viewRealEstate1.checkInput();
			if (nErrorCode != RealEstate1.ERROR_NONE) {
				selectView(VIEW_REAL_ESTATE);
				
				if (nErrorCode == RealEstate1.ERROR_MARKET_VALUE1)
					viewRealEstate1.edtMarketValue1.requestFocus();
				if (nErrorCode == RealEstate1.ERROR_ADDRESS1)
					viewRealEstate1.edtAddress1.requestFocus();
				
				return false;
			}
		}
			
		if (GlobalData.personalInfo.countRealState > 1) { 
			nErrorCode = viewRealEstate2.checkInput();
			if (nErrorCode != RealEstate2.ERROR_NONE) {
				selectView(VIEW_REAL_ESTATE);
				
				if (nErrorCode == RealEstate2.ERROR_MARKET_VALUE2)
					viewRealEstate2.edtMarketValue2.requestFocus();
				if (nErrorCode == RealEstate2.ERROR_ADDRESS2)
					viewRealEstate2.edtAddress2.requestFocus();
				
				return false;
			}
		}
		
		return checkLiabilitiles();
    }
    
    public boolean checkLiabilitiles() {
		nErrorCode = viewLiabilities.checkInput();
		if (nErrorCode != Liabilities.ERROR_NONE) {
			selectView(VIEW_LIABILITIES);
			
			if (nErrorCode == Liabilities.ERROR_BOARDING_RENTING_COST) {
				viewLiabilities.edtBoardingRentingCost.requestFocus();
			}
			if (nErrorCode == Liabilities.ERROR_OTHER_LIVING_EXPENSES) {
				viewLiabilities.edtOtherLivingExpenses.requestFocus();
			}
			if (nErrorCode == Liabilities.ERROR_OWING_CREDIT_LIMIT1) {
				viewLiabilities.edtOwingCreditLimet1.requestFocus();
			}
			if (nErrorCode == Liabilities.ERROR_MONTHLY_PAYMENT1) {
				viewLiabilities.edtMonthlyPayment1.requestFocus();
			}
			if (nErrorCode == Liabilities.ERROR_LENDER1) {
				viewLiabilities.edtLender1.requestFocus();
			}
			
			if (nErrorCode == Liabilities.ERROR_OWING_CREDIT_LIMIT2) {
				viewLiabilities.edtOwingCreditLimet2.requestFocus();
			}
			if (nErrorCode == Liabilities.ERROR_MONTHLY_PAYMENT2) {
				viewLiabilities.edtMonthlyPayment2.requestFocus();
			}
			if (nErrorCode == Liabilities.ERROR_LENDER2) {
				viewLiabilities.edtLender2.requestFocus();
			}
			
			return false;
		}
		
		return checkPersonalLoan();
    }
    
    public boolean checkPersonalLoan() {
    	
    	if (!m_isHasPersonalLoan)
    		return checkDetail();
    	
		nErrorCode = viewLiabilities.checkPersonalLoanInput();
		
		if (nErrorCode == Liabilities.ERROR_NONE) {
			return checkDetail();
		} else {
			new AlertDialog.Builder(SubmitActivity.this)
	        .setTitle("Question")
	        .setMessage("Most people would have loans & credit card repayments.Please list your expenses, thank you.")
	        .setPositiveButton("Ops,I forgot, let me fill in now", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) { 
	                // continue with delete
					selectView(VIEW_LIABILITIES);
					if (nErrorCode == Liabilities.ERROR_PERSONAL_OWING_CREDIT_LIMIT) {
						viewLiabilities.edtPersonalOwingCreditLimit.requestFocus();
					}
					if (nErrorCode == Liabilities.ERROR_PERSONAL_MONTHLY_PAYMENT) {
						viewLiabilities.edtPersonalMothlyPayment.requestFocus();
					}
					if (nErrorCode == Liabilities.ERROR_PERSONAL_LENDER) {
						viewLiabilities.edtPersonalLender.requestFocus();
					}
	            }
	         })
	         .setNegativeButton("I have no personal loans", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) { 
	                // continue with delete
	            	m_isHasPersonalLoan = false;
	            	checkDetail();
	            }
	         })
	         .show();
			return false;
		}
    }
    
    public boolean checkDetail() {
    	nErrorCode = viewDetail.checkInput();
		if (nErrorCode != EmploymentCompanyDetail.ERROR_NONE) {
			selectView(VIEW_DETAIL);
			
			if (nErrorCode == EmploymentCompanyDetail.ERROR_TITLE_IN_OCCUPATION) {
				viewDetail.edtTitleOccupation.requestFocus();
			}
			if (nErrorCode == EmploymentCompanyDetail.ERROR_EMPLOYER_NAME) {
				viewDetail.edtEmployerName.requestFocus();
			}
			if (nErrorCode == EmploymentCompanyDetail.ERROR_HR_CONTACT_NUMBER) {
				viewDetail.edtContactNumber.requestFocus();
			}
			if (nErrorCode == EmploymentCompanyDetail.ERROR_WORK_INCOME_AFTER_TAX) {
				viewDetail.edtWokIncomAfterTax.requestFocus();
			}
			if (nErrorCode == EmploymentCompanyDetail.ERROR_CENTRELINK_PENSION) {
				viewDetail.edtCentrelinkPension.requestFocus();
			}
			if (nErrorCode == EmploymentCompanyDetail.ERROR_ADD_OTHER_INCOME) {
				viewDetail.edtAddOtherIncome.requestFocus();
			}
			
			return false;
		}
		
		return checkCreditHistory();
    }
    
    public boolean checkCreditHistory() {
    	nErrorCode = viewCreditHistory.checkInput();
    	if (nErrorCode != CreditHistory.ERROR_NONE) {
    		selectView(VIEW_CREDIT_HISTORY);
    		
    		if (nErrorCode == CreditHistory.ERROR_TOTAL_OUTSTANDING) {
    			viewCreditHistory.edtTotalOutStandingValue.requestFocus();
    		}
    		return false;
    	}
    	
		sendDataProc();
//		goCompletePage();
		
    	return true;
    }
    
    public void sendDataProc() {

        try {
        	new MyAsyncTask(SubmitActivity.this).execute("signup");
        	
        } catch (Exception e) {
        	
        }
		
    }
    
    public static void stopLoading() {
    	
    }
    
	public void onClick(View v) {
		if (v == btnAsset) {
			selectView(VIEW_ASSET);
		}
		if (v == btnMotorVehicle) {
			selectView(VIEW_MOTOR_VEHICLE);
		}
		if (v == btnRealEstate) {
			selectView(VIEW_REAL_ESTATE);
		}
		if (v == btnLiabilities) {
			selectView(VIEW_LIABILITIES);
		}
		if (v == btnDetail) {
			selectView(VIEW_DETAIL);
		}
		if (v == btnCreditHistory) {
			selectView(VIEW_CREDIT_HISTORY);
		}
	}
}
