package com.example.morganfinance;

public class GlobalData {
	
	//// 				Home
	public static String itemsOfTitle[] = {"-Select-", "Mr", "Ms", "Miss", "Mrs", "Dr", "Other"};
	public static String itemsOfCitizeShip[] = {"-Select-", "Australian - Born in Australia", "Australian - Born overseas", "Permanent Resident (PR)",
			"New Zealand Citizen & Australian PR", "Working Visa(457)", "Others"};
	public static String itemsOfResidentialStatus[] = {"-Select-", "I am renting", "I am boarding", "I have a mortgage", "I own my home outright"};
	public static String itemsOfNumberDependant[] = {"0", "1", "2", "3", "4", "5"};
	public static String itemsOfLoanPurpose[] = {"-Select-", "Personal bills/purchases", "Rental bond", "Holiday/Wedding", "House renovation",
			"Debt consolidation", "Business related purposes"};
	
	
	////				Asset
	public static String itemsOfNumberMotorVehicle[] = {"None", "1", "2"};
	public static String itemsOfNumberRealEstate[] = {"None", "1", "2"};
	
	////				Motor Vehicle
	public static String itemsOfRegisteredOwnersNameForMortorVehicle[];
	
	public static String itemsOfRegisteredOwnersNameForRealEstate[];
	
	public static String itemsOfYear[] = {"-Select-", "2013", "2012", "2011", "2010",
			"2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000",
			"1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990",
			"1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980",
			"1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970",
			"1969", "1968", "1967", "1966", "1965", "1964"
	};
	
	//  Emploment or Company Details
	
	public static String itemsOfEmploymentStatus[] = {"Employee", "Employee + Centrelink Income", "Centrelink Income", "Self-Employed",
			"Company Director", "Contractor", "Cash Income", "Others"};
	public static String itemsOfTypeEmployment[] = {"F/T", "P/T", "Casual", "Temp."};
	
	public static String itemsOfPeriodly[] = {"Weekly", "Fortnightly", "Monthly"};
	
	public static String itemsOfCreditHistory[] = {"-Select-", "Clean (no default)", "1 default", "2 default", "3 or more defaults", 
			"Under bankruptcy", "Ex-bankrupt", "Under Part IX debt agreement", "Past Part IX debt agreement"};
	
	public static String itemsOfAddOtherIncome[] = {"- Other Income -", "2nd Job(Payslip", "Parent's income", 
		"Spouse's income", "Rental income", "Interest income", "Child Support(Court order)", 
		"Child Support(Private)", "Private arrangement"};
	
	public static String itemsOfDischargedDate[] = {"-Select-", "0 Year", "1 Year", "2 Years", "3 Years", "4 Years"
		, "5 Years", "6 Years", "7 Years", "8 Years", "9 Years", "10+ Years"};
	
	public static String itemsOfPurpose[] = {"-Select-", "1. Loan enquiry", "2. Reloan", "3. Application progress check"
			, "4 Customer feedback", "5. Repayment issue", "6. Others"};
	
	public static PersonalInfo personalInfo;
	
	public static void makeItemsOfRegisteredOwnersNameForMortorVehicle() {
		String items[] = new String[5];
		
		items[0] = "-Select-";
		items[1] = "Fully Registered by " + personalInfo.strFirstName1;
		items[2] = "Partially Registered by " + personalInfo.strFirstName1;
		items[3] = "Registered by my partner";
		items[4] = "Registered by someone else";
		
		itemsOfRegisteredOwnersNameForMortorVehicle = items;
 	}
	
	public static void makeItemsOfRegisteredOwnersNameForRealEstate() {
		String items[] = new String[4];
		
		items[0] = "-Select-";
		items[1] = "Fully Registered by " + personalInfo.strFirstName1;
		items[2] = "Partially Registered by " + personalInfo.strFirstName1;
		items[3] = "Registered by someone else";
		
		itemsOfRegisteredOwnersNameForRealEstate = items;
	}
	
	public static String strScreenType;
}
